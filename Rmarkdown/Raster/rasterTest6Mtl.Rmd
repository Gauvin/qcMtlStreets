---
title: "rasterTest6Mtl"
author: "Charles"
date: "July 6, 2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
  


# Librairies
 
```{r, include=FALSE}

library(tidyverse)
library(magrittr)
library(ggplot2)
library(mapdeck)
library(rayshader)
library(rgdal)
library(raster)
library(sf)
library(leaflet)
library(rayshaderDemo)

```
 
 
#Load raster of elevation
```{r}

miceadds::load.Rdata(filename = here::here("Data","Rdata","mtlRasterElevationFromBbox_14.Rdata"),
                     objname = "mtlRasterElevationFromBbox_14")

 
```
 
 
```{r}

raster::plot(mtlRasterElevationFromBbox_14)

 
```


```{r}

fileNameMtlNeigh <- here::here(file.path("Data","GeoData","MontrealNeighbourhoods","Quartiers_sociologiques_2014.shp"))
sfpMtl <- st_read(fileNameMtlNeigh)

montRoyalLong<- -73.5913127
montRoyalLat <- 45.5069527
montRoyal <- st_point(c(montRoyalLong,montRoyalLat)) 

shpMontRoyalBuff <- st_buffer(montRoyal,dist=0.05) %>% st_sfc %>% st_set_crs(st_crs(sfpMtl))
shpMontRoyalPoint <- montRoyal %>% st_sfc %>% st_set_crs(st_crs(sfpMtl))

idxTouch <-   st_intersects( sfpMtl, shpMontRoyalBuff) %>% as.numeric()
shpMontRoyalNeigh <- sfpMtl[!is.na(idxTouch),]



ggplot() + 
  geom_sf(data=shpMontRoyalNeigh, aes(color="red")) +
  geom_sf(data=shpMontRoyalBuff, alpha=0.5) + 
  geom_sf(data=shpMontRoyalPoint) + 
  ggtitle("Neighbourhoods around Mont-Royal")

 
  
```


```{r}



mtlRastCropped <- rayshaderDemo::crop_raster_shp(rasterData=mtlRasterElevationFromBbox_14,
                shpData=shpMontRoyalNeigh)


raster::plot(mtlRastCropped)
```


```{r}


matNeighMtl <- rayshaderDemo::raster_to_matrix(mtlRastCropped) 
rayshaderDemo::create_2D_map( matNeighMtl ,"mtlNeighMontRoyal.png")

```