---
title: "rasterTest4"
author: "Charles"
date: "July 2, 2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
 
 
 

# Librairies
 
```{r, include=FALSE}

library(tidyverse)
library(magrittr)
library(ggplot2)
library(mapdeck)
library(rayshader)
library(rgdal)
library(raster)
library(sf)
library(leaflet)


library(rayshaderDemo) #made package from rayshader-demo from github

library(gdalUtils)
library(RQGIS3)
```



#Load raster of elevation
```{r}

miceadds::load.Rdata(filename = here::here("Data","Rdata","qcRasterElevationFromBbox_14.Rdata"),
                     objname = "qcRasterElevationFromBbox_14")

```



#Try to get a smaller area first
```{r}


fileNameQcNeigh <- here::here(file.path("Data","GeoData","Neighbourhoods","vdq-quartier.shp"))
sfpQc <- st_read(fileNameQcNeigh)
bboxQcSubset <- st_bbox(sfpQc %>% filter(NOM %in% c( "Saint-Jean-Baptiste" )))

```

#Plot tests

```{r}

listNeigh <-  c("Saint-Roch", 
                "Saint-Jean-Baptiste", 
                "Vieux-Québec/Cap-Blanc/Colline parlementaire",
                "Montcalm")

#Use raster::intersect to get the extent from the original larger qcRasterElevationFromBbox_14
# + then crop over the desired neighbourhoods
qcRasterSubset <- intersect(qcRasterElevationFromBbox_14,
                            sfpQc %>%  filter(NOM %in% listNeigh ) )

#Transfer NOM to raster data
#Need to transform to sp object first and remove the z and m components
neighRaster <- rasterize(  sfpQc %>%  filter(NOM %in% listNeigh ) %>% st_zm %>% as("Spatial") , 
                           qcRasterSubset, 
                           'NOM')

```

##Overlay test for rasters with raster::plot
```{r}



plot(qcRasterSubset, col=heat.colors(10))
plot(neighRaster, add=T)


```

##Overlay test for rasters with raster::plot - 2
```{r}

#Same overlay test as preceding, but with order reversed
plot(neighRaster)
plot(qcRasterSubset, col=heat.colors(10), add=T)
 

```
##Overlay test for rasters with leaflet
```{r}

neighWithRasterLeaflet <- leaflet() %>% 
  addRasterImage(qcRasterSubset) %>% 
  addPolygons( data= sfpQc %>% st_zm )

(neighWithRasterLeaflet)

```

##Overlay test with ggplot + raster conversion to dataframe
```{r}

tictoc::tic("GGplot + raster test start")
qcRasterSubsetDf <- as.data.frame(qcRasterSubset, xy = TRUE)
print(dim(qcRasterSubsetDf))
ggplot()+
  geom_raster(data=qcRasterSubsetDf, aes(x=x,y=y,fill=layer) )
tictoc::toc( )

```


<!-- #Other tests - watershed - too heavy -->
<!-- ```{r} -->

<!-- library(here) -->
<!-- library(gdalUtils) -->
<!-- library(RQGIS3) -->

<!-- RQGIS3::set_env() -->

<!-- gdal_translate(here("Data", "GeoData", "Raster", "cdem_dem_021M_tif", "cdem_dem_021M.tif"), -->
<!--                here("Tmp", "dem_temp.tif"), -->
<!--                projwin = c(-71.2, 47.5, -70.5, 47)) -->

<!-- #get_usage(alg = "grass7:r.watershed") -->
<!-- run_qgis(alg = "grass7:r.watershed", -->
<!--          elevation = here("Tmp", "dem_temp.tif"), -->
<!--          threshold = 5000, -->
<!--          drainage = here("Tmp", "drain.tif")) -->

<!-- ``` -->