---
title: "mapdeckTest"
author: "Charles"
date: "June 8, 2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


# Librairies
 
```{r}

library(tidyverse)
library(magrittr)
library(ggplot2)
library(mapdeck)

```


# Read mapbox api key
 
```{r, include = F}

getMapBoxApiKey  <- function(dirPath,
                             fileName='mapBoxApiKey') {
  
  absPathApiKey <- file.path(dirPath, fileName)
  
  if(!file.exists(absPathApiKey)){
    GeneralHelpers::determineDirExistenceRecursively(absPathApiKey)
    stop("Fatal error, file does not exist")
  }
  
  apiKey <- read_file(absPathApiKey) %>% str_remove("\n")
 
  return(apiKey) 
}


apiKey <- getMapBoxApiKey(dirPath=here::here("apiKeys") )

```

#Read in the sf object

```{r}

miceadds::load.Rdata(filename = here::here("Data","Rdata","shpIncomeQcWithElevNeighFiltered.Rdata" ),
                     objname = "shpIncomeQcWithElevNeighFiltered")

```

```{r}
shpGraphElev %>% dplyr::select(x,y,elevation) %>% st_set_geometry(NULL) %>% reshape2::acast(.~x+y)

```

```{r}

#mapdeck(token = apiKey)
map <- mapdeck(token = apiKey, style = 'mapbox://styles/mapbox/dark-v9')%>% 
  add_polygon(
    data = shpIncomeQcWithElevNeighFiltered, 
    layer = "polygon_layer", 
    fill_colour = "v_CA16_2540_popWeighted"
  )

map
```


```{r}

<script src='https://api.mapbox.com/mapbox-gl-js/v1.0.0/mapbox-gl.js'></script>
<link href='https://api.mapbox.com/mapbox-gl-js/v1.0.0/mapbox-gl.css' rel='stylesheet' />

<div id='map' style='width: 400px; height: 300px;'></div>
<script>
mapboxgl.accessToken = "pk.eyJ1IjoiY2dhdXZpIiwiYSI6ImNqd29hMjhvNDB0NWU0YWxuMmJhMzU5cmsifQ.0LC70CvoyVzDXUElJC7dgg";
var map = new mapboxgl.Map({
container: 'map',
style: 'mapbox://styles/mapbox/streets-v11'
});
</script>  
  
```

