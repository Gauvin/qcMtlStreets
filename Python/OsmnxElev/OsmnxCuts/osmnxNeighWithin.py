import numpy as np
import pandas as pd

from OsmnxElev import *
from OsmnxElev.OsmnxCuts import *
from OsmnxElev.OsmnxCuts.osmnxNeighCut import *
from OsmnxElev.OsmnxGraphCreation import *


def getEdgesInSet(graph, dfEdgesCutset):

    #Get all edges in the graph
    #This will create a df with a startEndPair column
    dfAllEdges = getDfAllEdges(graph)

    if(np.isin(dfEdgesCutset.columns, 'startEndPair').any() == False):
        raise Exception("Fatal error in getEdgesInSet! the column startEndPair does not exist in dfEdgesCutset")

    #Compute the antijoin wrt to the cutset
    #We will end up with only edges that do not cross from one set to another
    dfAllEdgesInSet0 = pd.merge(left=dfAllEdges,
                                right=dfEdgesCutset,
                                how='left',
                                indicator=True,
                                on='startEndPair')



    dfAllEdgesInSet = dfAllEdgesInSet0.loc[dfAllEdgesInSet0._merge == 'left_only', :].drop(columns='_merge')

    #rename after merge
    dfAllEdgesInSet=dfAllEdgesInSet[ ['startNode_x','endNode_x','startEndPair','grade_abs_x']]

    dfAllEdgesInSet.rename(columns={'startNode_x': 'startNode',
                                    'endNode_x': 'endNode',
                                    'grade_abs_x':'grade_abs'},
                           inplace=True)

    #Add in the node neighbourhoods
    dfNodes=getDfAllNodes(graph)

    dfAllEdgesInSet=pd.merge( dfAllEdgesInSet,dfNodes,
              how='left',
              left_on='startNode',
              right_on="node").rename(columns={'Neighbourhood':'startNeigh',
                                                                         'elevation':'startElevation'})


    dfAllEdgesInSet=pd.merge( dfAllEdgesInSet,dfNodes,
              how='left',
              left_on='endNode',
              right_on="node").rename(columns={'Neighbourhood':'endNeigh',
                                                                       'elevation': 'endElevation'})

    dfAllEdgesInSet.drop(columns=['node_x','node_y'],inplace=True)

    return(dfAllEdgesInSet)



def getEdgesInSetNeigh(graph, listNeigh):

    #Get all the edges in the (directed cutset formed by the neighbourhood partition)
    dfEdgesCutset = getAllListEdgesDirectedMultigraph(graph=graph, listNeigh=listNeigh)

    #Add the startEndPair
    dfEdgesCutset['startEndPair'] = [(u, v) for u, v in dfEdgesCutset[['startNode', 'endNode']].values]

    #Call the generic function
    dfAllEdgesInSet=getEdgesInSet(graph, dfEdgesCutset)

    return(dfAllEdgesInSet,dfEdgesCutset)