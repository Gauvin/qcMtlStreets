import networkx as nx
import pandas as pd
import geopandas as gpd
import osmnx as ox
import matplotlib.pyplot as plt

from OsmnxElev import *


def getListEdgesCut(graph,nodeList1,nodeList2):

    edgesCut=nx.edge_boundary(graph, nodeList1, nodeList2 ) #this returns a generator

    listEdgesInCut =[ e for e in edgesCut] #watch out with the generators => not saved
    
    return(listEdgesInCut)

def getDfEdgesCut(graph,nodeList1,nodeList2):
    
    listEdgesInCut=getListEdgesCut(graph,nodeList1,nodeList2)
    
    dfEdgesInCut = pd.DataFrame(
        {"startNode" : [ listEdgesInCut[i][0] for i in range(len(listEdgesInCut))],
         "endNode" : [ listEdgesInCut[i][1] for i in range(len(listEdgesInCut))]
        }
    )
    
    return(dfEdgesInCut)


