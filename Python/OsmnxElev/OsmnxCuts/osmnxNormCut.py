 
import numpy as np
import pandas as pd
import geopandas as gpd
import os
import osmnx as ox
import networkx as nx


from OsmnxElev import *
from OsmnxElev.OsmnxGraphCreation import *

from sklearn.cluster import SpectralClustering


def getLargestConnectedComponent(graph):

    if(nx.is_directed(graph)==True):
        raise Exception("Fatal error in getLargestConnectedComponent, the graph is directed => input an undirected graph")

    connComp = [i for i in nx.connected_components(graph)]
    maxComp = np.argmax([len(i) for i in connComp])
    listNodes = connComp[maxComp]

    print("In getLargestConnectedComponent, returning the largest connected component with ", \
          len(listNodes), " nodes vs the " , len(graph.nodes()), " nodes originally present in the entire graph")

    graphSubgraphLargestCC = nx.induced_subgraph(graph, listNodes)

    if(nx.is_connected(graphSubgraphLargestCC)==False):
        raise ("Fatal error in getLargestConnectedComponent => the resulting maximum largest connected component is still disconnected")

    return(graphSubgraphLargestCC)





def computeSpectralClustFromGraphQc(listNeigh=["Saint-Jean-Baptiste", \
                                                "Saint-Roch" , \
                                                "Vieux-Québec/Cap-Blanc/Colline parlementaire",\
                                                "Montcalm",\
                                                "Saint-Sauveur"],
                                     weightCol=None,
                                     numClust=2,
                                     simplyfyGraph=False):

    #get the induced subgraph
    graphQc = loadGraphElevGradesNeighQc()
    graphQcSubset, listNodes = getSubgraphListNeighOneGraph(graphQc, listNeigh)

    if(simplyfyGraph==True):
        print("In computeSpectralClustFromGraphQc => Simplifying graph")
        graphQcSubset=ox.simplify_graph(graphQcSubset)
        graphQcSubset=ox.add_edge_grades(graphQcSubset)
    else:
        print("In computeSpectralClustFromGraphQc => Using raw (unsimplfied) graph")

    graphCC, adjMat, nodeAssign =computeSpectralClustFromGraph(graphQcSubset,numClust,weightCol)




    return(graphCC, adjMat, nodeAssign)




def computeSpectralClustFromGraph(graph,
                                  numClust=2,
                                  weightCol=None):

    #Make undirected
    graph = graph.to_undirected()

    #Get the largest connected component
    graphCC=getLargestConnectedComponent(graph)

    #Get the adjacency matrix
    #If weightCol is None then will use the simple adj matrix
    adjMat=getAdjMatrixFromGraph(graphCC,
                                 weightCol)

    print("In computeSpectralClustFromGraph, using ", numClust, " clusters")

    nodeAssign = SpectralClustering(n_clusters=numClust, affinity="precomputed").fit_predict(adjMat)


    return(graphCC, adjMat, nodeAssign)