import re
import numpy as np
import pandas as pd
import geopandas as gpd
import os
import osmnx as ox
import pickle
import networkx as nx

from shapely.geometry import Point
from shapely.geometry import Polygon, MultiPolygon
from descartes import PolygonPatch
from matplotlib.collections import PatchCollection

from OsmnxElev import *
from OsmnxElev.OsmnxCuts import *
from OsmnxElev.OsmnxGraphCreation import *

################################ Utils ################################

def getIdxLowerGraph(i,j):
    
    if i < j:
        return(i,j)
    else:
        return(j,i)
    
    



def getListEdgesCutNeigh(graph,neigh1,neigh2):
    listNeigh1=getListNodesNeigh(graph,neigh1)
    listNeigh2=getListNodesNeigh(graph,neigh2)
    
    return(getListEdgesCut(graph,listNeigh1, listNeigh2 ))


def addNeighPairIdxEdgeDf(dfEdges):
    dfEdges = addNeighIdxFromNeighName(dfEdges)
    dfEdgesWithNeigh = addPairIdxEdgeDf(dfEdges)

    return (dfEdgesWithNeigh)


def getNeighNamePairFromNeighIdxPair(i, dfEdges, dfNodes, useDirected=False):



    if (useDirected):
        pairCol = dfEdges.pairIdxUndirected
    else:
        pairCol = dfEdges.pairIdxUndirected
    try:
        # Get the index
        startIdxUndirected = pairCol[i][0]
        endIdxUndirected = pairCol[i][1]

        # Get the nwighbourhood names
        neigh1 = dfNodes.loc[dfNodes['index'] == startIdxUndirected,]["neigh"].values[0]
        neigh2 = dfNodes.loc[dfNodes['index'] == endIdxUndirected,]["neigh"].values[0]

        neighPair = (neigh1, neigh2)
    except Exception as e:
        neighPair = (np.nan, np.nan)
        print("Fatal error getting undirecte neigh for index ", i, " - ", e)


    return (neighPair)



def addNeighNamePairFromNeighIdxPair(dfEdges,useDirected=False):

    colName =  'neighPairDirected' if useDirected else  'neighPairUndirected'

    #Get the nodes + rename the index column
    dfNodes = dfEdges[['startNeigh', 'startIdx']].drop_duplicates()
    dfNodes.rename(columns={'startIdx': 'index', 'startNeigh': 'neigh'}, inplace=True)

    dfEdges[colName] = [getNeighNamePairFromNeighIdxPair(i, dfEdges, dfNodes, useDirected=useDirected) for i, row in
                                      dfEdges.iterrows()]

    return(dfEdges)


def addNeighIdxFromNeighName(dfEdges):
    if (np.isin(["startNeigh", "endNeigh"], dfEdges.columns).all() == False):
        raise Exception("Fatal error in addNeighIdxFromNeighName => need startNeigh and endNeigh")

    # Add the neighbourhood + neigh idx
    listNeighSorted = np.sort(np.unique(np.concatenate([dfEdges.startNeigh.unique(), dfEdges.endNeigh.unique()])))
    dfNeighWithIdx = pd.DataFrame({"neigh": listNeighSorted, "neighIdx": [i for i in range(len(listNeighSorted))]})

    dfEdgesWithNeigh = dfEdges
    dfEdgesWithNeigh = pd.merge(dfEdgesWithNeigh, dfNeighWithIdx,
                                how="left",
                                left_on="startNeigh", right_on="neigh")

    dfEdgesWithNeigh = pd.merge(dfEdgesWithNeigh, dfNeighWithIdx,
                                how="left",
                                left_on="endNeigh", right_on="neigh")

    return (dfEdgesWithNeigh)


def addNeighIdxFromPairCol(dfEdges,
                           pairIdxColStr='index',
                           startIdxName="startIdx",
                           endIdxName="endIdx"):
    dfEdges[startIdxName] = [i[0] for i in dfEdges[pairIdxColStr]]
    dfEdges[endIdxName] = [i[1] for i in dfEdges[pairIdxColStr]]

    return (dfEdges)

def addNeighIdxAndNeighNamesFromPairCol(dfEdges,
                                        dfNeighWithIdx,
                                        pairIdxColStr='index',
                                        startIdxName="startIdx",
                                        endIdxName="endIdx"):
    dfEdges=addNeighIdxFromPairCol(dfEdges,
                                   pairIdxColStr,
                                   startIdxName,
                                   endIdxName)

    dfEdges=pd.merge(dfEdges, dfNeighWithIdx,
             how="left", on=["startIdx", "endIdx"])

    return(dfEdges)

def addPairIdxEdgeDf(dfEdgesWithNeigh,
                     startIdxName="neighIdx_x",
                     endIdxName="neighIdx_y"
                     ):
    if (np.isin([startIdxName, endIdxName], dfEdgesWithNeigh.columns).all() == False):
        print("In addPairIdxEdgeDf => ", startIdxName, " or ", endIdxName, " not present : adding them")
        dfEdgesWithNeigh = addNeighIdxFromNeighName(dfEdgesWithNeigh)

    # Get the pair (i,j) index forming the directed arc
    dfEdgesWithNeigh["pairIdxDirected"] = [(idxx, idxy) for idxx, idxy in
                                           dfEdgesWithNeigh[[startIdxName, endIdxName]].values]
    # Same for undirected => only return the lower diag
    dfEdgesWithNeigh["pairIdxUndirected"] = [getIdxLowerGraph(idxx, idxy) for idxx, idxy in
                                             dfEdgesWithNeigh[["neighIdx_x", "neighIdx_y"]].values]

    return (dfEdgesWithNeigh)



################################ Directed/undirected neighbourhood multigraphs ################################

'''
Get the directed edges between all neigh first, then convert to undirected (weighted) graph 
where the edge weight is the number of unique pairs (i,j) i < j (so that each directed arc contributes 1 to the weight of the edge)

This returns a multigraph if seen from the perspective of neighbourhoods

'''

def getAllListEdgesDirectedMultigraph(graph,listNeigh,listFeat=["grade","grade_abs"], onlyAcrossNeigh=False):

    if(onlyAcrossNeigh==True):
        print("In getAllListEdgesDirectedMultigraph => only considering edges accross DIFFERENT neighbourhoods")

    #Add the required features upfront
    if (np.any([re.match(".*bearing.*", i) is not None for i in listFeat])):
        print("In getAllListEdgesDirectedMultigraph => adding edge bearings")
        graph = ox.add_edge_bearings(graph)
    elif (np.any([re.match(".*grades.*", i) is not None for i in listFeat])):
        print("In getAllListEdgesDirectedMultigraph => adding edge grades")
        graph = ox.add_edge_grades(graph, add_absolute=True)
    elif (np.any([re.match(".*length.*", i) is not None for i in listFeat])):
        print("In getAllListEdgesDirectedMultigraph => adding edge lengths")
        graph = ox.add_edge_lengths(graph)

    #Consider all nC2 pairs
    listAllPairs=[]
    dfEdgesDirected=pd.DataFrame()
    for i in listNeigh:
        for j in listNeigh:

            #If we only want edges crossing neighbourhoods
            if onlyAcrossNeigh==True and i == j :
                continue

            else:
                
                listEdgesInCut=getListEdgesCutNeigh(graph,i,j)
                listAllPairs+=listEdgesInCut 
                
                dfEdgesInCut = pd.DataFrame({"startNode" : [ listEdgesInCut[i][0] for i in range(len(listEdgesInCut))],
                                             "startNeigh": [i for l in range(len(listEdgesInCut)) ],
                                             "endNode" : [ listEdgesInCut[i][1] for i in range(len(listEdgesInCut))],
                                             "endNeigh": [j for l in range(len(listEdgesInCut)) ]
                                            }
                                           )
                #Add the features as well separately - could be done with the initial df creation, but this is more robust if we make an error in the features
                dictFeat={}
                for f in listFeat:
                    try:
                        dictFeat[f] = [ graph.edges[ (listEdgesInCut[i][0], listEdgesInCut[i][1], 0) ][f] for i in range(len(listEdgesInCut))]
                    except Exception as e:
                        print("Fatal error trying to add feature ", f, " " , str(e))



                dfFeat=pd.DataFrame.from_dict(dictFeat)
                dfEdgesInCut=pd.concat([dfEdgesInCut,dfFeat],axis=1)

                dfEdgesDirected=pd.concat([dfEdgesDirected,dfEdgesInCut])
    
    
    #Now add the neigh + neigh idx + the (neigh1,neigh2) pair
    dfEdgesDirected=addNeighPairIdxEdgeDf(dfEdgesDirected)
               
    if(dfEdgesDirected.shape[0] != len(listAllPairs)):
        print("Watch out, some neigh could not be matched, or some were matched to many endpoints, number of records in df: ",
             dfEdgesDirected.shape[0], " -- number of edges: " , len(listAllPairs))

    # Remove duplicate columns created by the merge + rename for compatiblity
    dfEdgesDirected = dfEdgesDirected.rename(columns={"neighIdx_x": "startIdx", "neighIdx_y": "endIdx"}  ).drop(
        columns=['neigh_x', 'neigh_y'])


    #Add the neighbourhood name pairs
    #Add both the directed and undirected name pairs
    dfEdgesDirected=addNeighNamePairFromNeighIdxPair(dfEdgesDirected,useDirected=False)
    dfEdgesDirected = addNeighNamePairFromNeighIdxPair(dfEdgesDirected,useDirected=True)

    return (dfEdgesDirected)
    #return(listAllPairs,dfEdgesAll) useless : listEdges=[ (row["startNode"],row["endNode"]) for k, row in dfEdgesDirected.iterrows()]


'''
Get the directed edges between all neigh first, then convert to undirected (weighted) graph 
where the edge weight is the number of unique pairs (i,j) i < j (so that each directed arc contributes 1 to the weight of the edge)

This returns a multigraph if seen from the perspective of neighbourhoods

'''

#@@@ TODO @@@ : make this fct more flexible + same behaviour as getAllUndirectedWeightedEdgesNeighGraph
def getAllUndirectedEdges(dfEdgesDirected,
                          startIdxName="startIdx",
                          endIdxName="endIdx"
                          ):
    # Get the list of neigh
    listNeighSorted = dfEdgesDirected.startNeigh.unique()
    dfNeighWithIdx = pd.DataFrame({"neigh": listNeighSorted, "neighIdx": [i for i in range(len(listNeighSorted))]})

    if (np.isin([startIdxName, endIdxName], dfEdgesDirected.columns).all() == False):
        raise Exception("Fatal error in getAllUndirectedEdges, missing startIdx, endIdx")

    # Get the neigh idx
    dfEdgesUndirected = pd.DataFrame(dfEdgesDirected.pairIdxUndirected.value_counts()).reset_index()
    dfEdgesUndirected = addNeighIdxFromPairCol(dfEdgesUndirected, "index", startIdxName, endIdxName)

    # Re-add the neighbourhood to the undirected edges
    dfEdgesUndirected = pd.merge(dfEdgesUndirected, dfNeighWithIdx,
                                 how="left",
                                 left_on="startIdx", right_on="neighIdx")

    dfEdgesUndirected = pd.merge(dfEdgesUndirected, dfNeighWithIdx,
                                 how="left",
                                 left_on="endIdx", right_on="neighIdx")

    # Remove duplicate columns created by the merge + rename for compatiblity
    dfEdgesUndirected = dfEdgesUndirected.rename(columns={"neigh_x":"startNeigh",
                                                          "neigh_y" : "endNeigh",
                                                          "pairIdxUndirected" : "count",
                                                          "index": "pairIdxUndirected"}).drop(columns=['neighIdx_x', 'neighIdx_y'])

    return (dfEdgesUndirected)

################################ Weighted directed/undirected neighbourhood graph functions ################################

def getAllDirectedWeightedEdgesNeighGraph(graph, listNeigh, listFeat, aggFct):

    dfListEdgesMultigraph=getAllListEdgesDirectedMultigraph(graph, listNeigh, listFeat)

    dfEdgesDirectedWeighted = pd.DataFrame(
        dfListEdgesMultigraph.reset_index().groupby(["pairIdxDirected"]).count()["index"]
    ).rename(columns={"index": "count"}).reset_index()


    #Merge in the features
    print("In getAllDirectedWeightedEdgesNeighGraph => using the agg function ",
          aggFct, " on the following features:\n", ",".join(listFeat))

    listFeat=np.unique(listFeat)
    listFeatInCols=np.array(listFeat)[ np.isin(listFeat, dfListEdgesMultigraph.columns)]
    listFeatInColsAndInd=np.append(listFeatInCols , ["pairIdxDirected"] )

    if(np.isin(listFeat, listFeatInCols).all()==False):
        print("Warning in getAllDirectedWeightedEdgesNeighGraph! ",
              np.sum(~np.isin(listFeat, listFeatInCols)),
              " features are not present in the columns")

    dfEdgesDirectedWeightedWithFeat =  dfListEdgesMultigraph.groupby(["pairIdxDirected"]).agg(aggFct).reset_index()[listFeatInColsAndInd]

    dfEdgesDirectedWeighted=pd.merge(dfEdgesDirectedWeighted,dfEdgesDirectedWeightedWithFeat,
                                     how="left",on="pairIdxDirected")

    dfNeighWithIdx = dfListEdgesMultigraph[["startNeigh", "startIdx", "endNeigh", "endIdx"]].drop_duplicates()

    dfEdgesDirectedWeighted=addNeighIdxAndNeighNamesFromPairCol(dfEdgesDirectedWeighted,
                                        dfNeighWithIdx,
                                        "pairIdxDirected")

    return(dfEdgesDirectedWeighted)

#make this fct call getAllUndirected
def getAllUndirectedWeightedEdgesNeighGraph(graph, listNeigh, listFeat, aggFct):

    dfListEdgesMultigraph = getAllListEdgesDirectedMultigraph(graph, listNeigh, listFeat)

    dfEdgesUndirectedWeighted = pd.DataFrame(
        dfListEdgesMultigraph.reset_index().groupby(["pairIdxUndirected"]).count()["index"]
    ).rename(columns={"index": "count"}).reset_index()

    # Merge in the features
    print("In getAllDirectedWeightedEdgesNeighGraph => using the agg function ",
          aggFct, " on the following features:\n", ",".join(listFeat))

    listFeat = np.unique(listFeat)
    listFeatInCols = np.array(listFeat)[np.isin(listFeat, dfListEdgesMultigraph.columns)]
    listFeatInColsAndInd = np.append(listFeatInCols, ["pairIdxUndirected"])

    if (np.isin(listFeat, listFeatInCols).all() == False):
        print("Warning in getAllUndirectedWeightedEdgesNeighGraph! ",
              np.sum(~np.isin(listFeat, listFeatInCols)),
              " features are not present in the columns")

    dfEdgesUndirectedWeightedWithFeat = dfListEdgesMultigraph.groupby(["pairIdxUndirected"]).agg(aggFct).reset_index()[
        listFeatInColsAndInd]

    dfEdgesUndirectedWeighted = pd.merge(dfEdgesUndirectedWeighted, dfEdgesUndirectedWeightedWithFeat,
                                       how="left", on="pairIdxUndirected")

    dfNeighWithIdx = dfListEdgesMultigraph[["startNeigh", "startIdx", "endNeigh", "endIdx"]].drop_duplicates()

    dfEdgesUndirectedWeighted=addNeighIdxAndNeighNamesFromPairCol(dfEdgesUndirectedWeighted,
                                        dfNeighWithIdx,
                                        "pairIdxUndirected")

    return (dfEdgesUndirectedWeighted)


################################ Quebec specific neighbourhood graph functions ################################
    
def getAllDirectedWeightedEdgesNeighGraphQc(listNeigh=["Saint-Jean-Baptiste",\
                                                       "Saint-Roch", \
                                                       "Montcalm",\
                                                       "Vieux-Québec/Cap-Blanc/Colline parlementaire"],
                                            listFeat=["grade_abs","abs"],
                                            aggFct="sum"):
    
    #osmnx graph with neigh
    graphQc=loadGraphElevGradesNeighQc()

    graphQcInduced,listNodes=getSubgraphListNeighOneGraph(graphQc,listNeigh)

    return(getAllDirectedWeightedEdgesNeighGraph(graphQcInduced,listNeigh,listFeat,aggFct))
    

def getAllUndirectedWeightedEdgesNeighGraphQc(listNeigh=["Saint-Jean-Baptiste",\
                                                         "Saint-Roch", \
                                                         "Montcalm",\
                                                         "Vieux-Québec/Cap-Blanc/Colline parlementaire"],
                                              listFeat=["grade_abs", "abs"],
                                              aggFct="sum"):
    
    #osmnx graph with neigh
    graphQc=loadGraphElevGradesNeighQc()

    graphQcInduced,listNodes=getSubgraphListNeighOneGraph(graphQc,listNeigh)

    return (getAllUndirectedWeightedEdgesNeighGraph(graphQcInduced, listNeigh, listFeat, aggFct))

    

def getAllListEdgesDirectedMultigraphQc(listNeigh=["Saint-Jean-Baptiste",\
                              "Saint-Roch", \
                             "Montcalm",\
                              "Vieux-Québec/Cap-Blanc/Colline parlementaire"]):

    #osmnx graph with neigh
    graphQc=loadGraphElevGradesNeighQc()

    graphQcInduced,listNodes=getSubgraphListNeighOneGraph(graphQc,listNeigh)

    return( getAllListEdgesDirectedMultigraph(graphQcInduced,listNeigh) )