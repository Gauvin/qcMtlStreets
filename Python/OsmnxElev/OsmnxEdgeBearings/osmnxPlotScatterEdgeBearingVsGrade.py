
import seaborn as sns
import pandas as pd



def getBearingGrade(dfEdges, neighPairFilter):

    dfEdges['neighPairUndirected'] == neighPairFilter
    arrayVals = dfEdges.loc[dfEdges['neighPairUndirected'] == neighPairFilter, ['bearing', 'grade_abs']].values
    bear, grade = arrayVals[:, 0], arrayVals[:, 1]

    if (len(bear) != len(grade)):
        raise Exception("fatal error in getBearingGrade => bearings and grade not same length - check na presence")

    return (bear, grade)




def plotKDEBearingVsGrade(dfEdges, neighPairFilter):

    bear, grade = getBearingGrade(dfEdges, neighPairFilter)

    fig = sns.kdeplot(bear, grade)
    fig.set_title(neighPairFilter)
    fig.set(xlabel="bearings", ylabel="grade")

    return (fig)




def plotKDEScatterBearingVsGrade(dfEdges, neighPairFilter):

    plotKDEBearingVsGrade(dfEdges, neighPairFilter)
    bear, grade = getBearingGrade(dfEdges, neighPairFilter)
    plot = sns.scatterplot(bear, grade)

    return(plot)