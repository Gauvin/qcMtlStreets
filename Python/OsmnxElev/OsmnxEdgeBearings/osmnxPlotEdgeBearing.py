
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import osmnx as ox
import networkx as nx

def plotEdgeBearing(graph, numBins=30):

    #Make sure the bearings have been computed
    if(len(nx.get_edge_attributes(G=graph,name="bearings"))==0):
        print("Computing edge bearings")
        graph=ox.add_edge_bearings(graph)
    
    #Get the bearings into a pandas series
    bearings=pd.Series([data["bearing"] for u,v,k,data in graph.edges(data=True,keys=True)])
    
    #Get the bins
    count, division = np.histogram(bearings,
                                   bins=[ang*360/numBins for ang in range(0,numBins+1)])

    #Plot the polar graph
    width=2*np.pi/numBins
    
    division = division[0:-1]
    
    ax = plt.subplot(111,projection="polar")
    ax.set_theta_zero_location("N")
    ax.set_theta_direction("clockwise")

    bars=ax.bar(division*np.pi/180-width*0.5,count,width)


    return(bars,ax)