
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import osmnx as ox

from OsmnxElev.OsmnxCuts import *

def plotBearingVsGrade (dfEdges,
                        neighPairUndirected=None,
                        nBearings=30, bucketBearings=None):

    '''
    Plot a polar stacked histogram where the height of each bar is the count of each edge bearing with a given edge grade

    :param dfEdges pandas df with columns bearingBucketRep, grade_abs
    :param neighPairUndirected : pair of form ('Saint-Jean-Baptiste', 'Saint-Jean-Baptiste') to filter on
    :param nBearings: int giving the number of bars
    :param bucketBearings: tuple of histogram divisions, used if bucketBearings is not none, else n used to create the div automatically

    '''

    if(~np.any(np.isin(['bearing'], dfEdges.columns))):
        raise Exception("Fatal error in plotBearingVsGrade, no edge bearing => compute them first ")

    print("In plotBearingVsGrade, considering the following pair of neighbourhoods in grade vs bearing plot ", neighPairUndirected,
          " using ", nBearings , " buckets")


    #Compute appropriate bearing buckets for bearings if necessary
    if bucketBearings is None:
        print("No bucketBearings using ",  nBearings , " buckets of size 2pi/", nBearings  )
        bucketBearings = [ang*360/nBearings for ang in range(0,nBearings+1)]
    else:
        print("Considering bucketBearings used as input")

    dfEdges['bucketBearings'] = pd.cut(dfEdges['bearing'], bins=bucketBearings)


    #Compute appropriate grade buckets for bearings if necessary

    # Get the edge grade quantiles over the entire population to compare accross neighbourhoods
    # use the first 2 deciles for low 0-1, 1-2
    # the next 4 decile for med 4-5, 5-6, 6-7, 7-8
    # and the top 2 deciles for high 8-9, 9-10 , where len(quant) - 1 == 10
    quantGrades = dfEdges.grade_abs.quantile([i / (10) for i in range(10 + 1)]).values
    bucketGrades = [quantGrades[0]-10**-4, quantGrades[2], quantGrades[8], quantGrades[10] + 10**-4]
    dfEdges['gradeBuckets']=pd.cut(dfEdges.grade_abs,
                                   bins=bucketGrades)

    print("Here are the grade buckets considered: ", quantGrades)

    if(np.sum( dfEdges.gradeBuckets.isna())):
        print("Watch out! created NaNs when bucketing edge grade_abs")

    #Add the grade colors
    tupleGradeBuckets = dfEdges['gradeBuckets'].unique()
    nGrades = len(tupleGradeBuckets)
    cmInf = cm.get_cmap('inferno', nGrades)
    dfCols = pd.DataFrame({'gradeBuckets': tupleGradeBuckets,
                           #'gradeBucketColor': [cmInf(i) for i in range(nGrades)]})
                          'gradeBucketColor': [cmInf((nGrades-1) - i) for i in range(nGrades)] })         #Reverse the traversal: start with the lightest/flattest first
    dfEdges = pd.merge(dfEdges, dfCols,
                           how='left',
                           on="gradeBuckets")



    #Get only edges in neigh pair
    if neighPairUndirected is not None:
        if (~np.all( np.isin(neighPairUndirected, dfEdges.neighPairUndirected.unique()))): #we are comparing pairs so need np.all not np.any
            raise Exception("Fatal error in plotBearingVsGrade => the undirected neighbourhood pair ", neighPairUndirected, " is not present in dfEdges"  )
        print("Filtering on pair of neighbourhoods: ", neighPairUndirected)
        dfEdgesNeigh = dfEdges.loc[dfEdges.neighPairUndirected == neighPairUndirected,]
    else:
        print("Considering all pairs of neighbourhoods: ")
        dfEdgesNeigh = dfEdges


    # Set the polar graph params
    width = 2 * np.pi / nBearings
    ax = plt.subplot(121, projection='polar')
    ax.set_theta_zero_location('N')
    ax.set_theta_direction('clockwise')

    #Get the edge grade quantiles over the entire population to compare accross neighbourhoods
    #use the first 2 deciles for low 0-1, 1-2
    # the next 4 decile for med 4-5, 5-6, 6-7, 7-8
    # and the top 2 deciles for high 8-9, 9-10 , where len(quant) - 1 == 10
    countsAllGrades, divAllGrades = np.histogram(dfEdgesNeigh.bearing,
                                                 bins=bucketBearings)
    divAllGrades = divAllGrades[0:-1]


    # Plot the count for each grade bucket
    bars = ax.bar(divAllGrades * np.pi / 180 - width * 0.5,
                  countsAllGrades,
                  width=width,
                  bottom=0.0)

    # Second graph
    ax = plt.subplot(122, projection='polar')
    ax.set_theta_zero_location('N')
    ax.set_theta_direction('clockwise')


    # Iterate over all possible grade categories/buckets/percentiles
    tupleGradeBuckets=tupleGradeBuckets.sort_values() #sort them first from smallest/flattest
    numGradeTuples=len(tupleGradeBuckets)

    #For the cumul bottom and counts
    countList = [[0] * (len(bucketBearings)-1)] * 3
    bottomList = [[0] * (len(bucketBearings)-1)] * 3
    countGradeSum  = [[0] * (len(bucketBearings)-1)] * 3

    for k in range(numGradeTuples):

        dfEdgesNeighGrade = dfEdgesNeigh.loc[dfEdgesNeigh['gradeBuckets'] == tupleGradeBuckets[k],]
        counts, div = np.histogram(dfEdgesNeighGrade.bearing,
                                   bins=bucketBearings)

        # use the previous cumulative sum as bottom for next iteration
        if(k>=1):
            bottomList[k] = bottomList[k-1] + countList[k-1]

        div = div[0:-1]
        countList[k] = counts
        countGradeSum += counts
        print("Here is the count for " , tupleGradeBuckets[k] , " : ", counts)



        # Plot the count for each grade bucket
        bars = ax.bar(div * np.pi / 180 - width * 0.5,
                      height=countList[k], #vector
                      width=width,
                      bottom=bottomList[k], #vector
                      color=dfCols.gradeBucketColor[k], #scalars should work fine here
                      label=tupleGradeBuckets[k])

    #Don't forget the legend (with larger text size)
    plt.legend(tupleGradeBuckets,
               loc='lower right', bbox_to_anchor=(0, 0., 0, 0),
               prop={'size': 15})

    #Make the picture larger
    frame = plt.gcf()
    frame.set_size_inches(10, 11)



    if (~np.allclose( countsAllGrades, countGradeSum, rtol=1e-02, atol=1e-04)) :

        print("Fatal error, the sum of counts over each edge grade does not add up to total sum of edges: \n",
              countsAllGrades, " vs ", countGradeSum, " diff (all-sum): ", countsAllGrades - countGradeSum)


    return(ax)