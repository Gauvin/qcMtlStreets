
import os
import pickle
import osmnx as ox
import geopandas as gpd
import numpy as np

from OsmnxElev.OsmnxGraphCreation.osmnxGetNodesInNeigh import *
from OsmnxElev.OsmnxGraphCreation.osmnxBbox import *
from OsmnxElev.OsmnxGraphCreation.osmnxCreateGraphElevGradesNeigh import *

from googleApiKey import *
 

def loadGraphElev(filePathPickle,place):

    if os.path.isfile(filePathPickle):
        print("File exists => reading graph")
        pickle_in = open(filePathPickle,"rb")
        graphWithElevation = pickle.load(pickle_in)
    else:
        print("File does not exists => calling google api")
        graph = ox.graph_from_place(place,
                                      simplify=False,
                                      retain_all=True)

        graphWithElevation=ox.add_node_elevations(graph, api_key=googleKey)
        pickle_out = open(filePathPickle,"wb")
        pickle.dump(graphWithElevation, pickle_out)
        pickle_out.close()
        
    return(graphWithElevation)


def loadGraphElevGradesNeigh(filePathPickle, filePathElev, place, shpNeigh, listNeigh ,varnameID="NOM"):


    if os.path.isfile(filePathPickle):
        print("File exists => reading graph")
        pickle_in = open(filePathPickle, "rb")
        graphWithElevation = pickle.load(pickle_in)
    else:
        print("File does not exists => adding elev, grades and neigh")
        graphWithElevation=loadGraphElev(filePathElev , place)

        #Add edge grades
        graphWithElevationGrades = ox.add_edge_grades(graphWithElevation)


        #Add the neighoburhoods to each node
        dfPointsSubsetInPoly = getPointsInPoly(graphWithElevationGrades, shpNeigh, listNeigh)

        setPointAllNeighbourhood(graphWithElevationGrades,
                                 dfPointsSubsetInPoly,
                                 shpNeigh,
                                 varnameID)


        pickle_out = open(filePathPickle, "wb")
        pickle.dump(graphWithElevationGrades, pickle_out)
        pickle_out.close()

    return (graphWithElevation)



def loadGraphElevGradesNeighQc():

    shpQcCity = gpd.read_file(os.path.join(ROOT_DIR, "Data", "GeoData", "Neighbourhoods", "vdq-quartier.shp"))
    listNeigh = shpQcCity.NOM
    shpNeighFiltered = shpQcCity[np.isin(shpQcCity["NOM"], listNeigh)]

    filePath=os.path.join(ROOT_DIR,"Data","Pickle", "graphElevationGradesNeighQc.pickle")
    filePathElev = os.path.join(ROOT_DIR, "Data", "Pickle", "graphElevationQc.pickle")
    grapQcElevGradesNeigh=loadGraphElevGradesNeigh(filePath,
                                    filePathElev,
                                    ['Québec city, canada'],
                                    shpNeighFiltered,
                                    listNeigh)

    return(grapQcElevGradesNeigh)


def loadGraphElevGradesNeighMtl():

    shpMtl = gpd.read_file(os.path.join(ROOT_DIR, "Data", "GeoData", "MontrealNeighbourhoods", "Quartiers_sociologiques_2014.shp"))
    listNeigh = shpMtl.Q_socio
    shpNeighFiltered = shpMtl[np.isin(shpMtl["Q_socio"], listNeigh)]

    filePath=os.path.join(ROOT_DIR,"Data","Pickle", "graphElevationGradesNeighMtl.pickle")
    filePathElev = os.path.join(ROOT_DIR, "Data", "Pickle", "graphElevationMtl.pickle")
    grapMtlElevGradesNeigh=loadGraphElevGradesNeigh(filePath,
                                    filePathElev,
                                    ['Montréal, Canada'],
                                    shpNeighFiltered,
                                    listNeigh,
                                     "Q_socio" )

    return(grapMtlElevGradesNeigh)