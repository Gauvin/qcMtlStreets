import pandas as pd
import re
import osmnx as ox
import geopandas as gpd


from OsmnxElev.OsmnxGraphCreation.osmnxGetNodesInNeigh import *
from OsmnxElev.OsmnxGraphCreation.osmnxCreateGraphElevGradesNeigh import *


def getSafeNeigh(n,dat,quiet=True):
    try:
        neigh=dat["Neighbourhood"]
    except Exception as e:
        if(quiet==False):
            print("Fatal error at node " , n , dat , str(e))
        neigh=""
        
    return(neigh)


def getListNodesNeigh(graph,neigh,quiet=True):
    
    listNodes=[n for n, dat in graph.nodes.data() if re.match(".*" + neigh + ".*", getSafeNeigh(n,dat,quiet)) is not None ]

    return(listNodes)
    
def getSubgraphNeigh(graph,neigh,quiet=True):
    
    listNodes=getListNodesNeigh(graph,neigh,quiet)
    
    graphInduced=ox.utils.induce_subgraph(graph,listNodes)
    
    return(graphInduced,listNodes)
    
    
def getSubgraphNeighWithEdgesAndFeat(graph,neigh,featuresExtractListNames,quiet=True):
    
    graphInduced,listNodes=getSubgraphNeigh(graph,neigh,quiet)
    
    dfEdges=getEdgesFromGraph(graphInduced,featuresExtractListNames)
    
    return(graphInduced,listNodes,dfEdges)



def getSubgraphListAllNeighWithEdgesGrade(graph,listRawNeigh=None,quiet=True):
    
    if listRawNeigh is None:
        listRawNeigh=(list(set([ getSafeNeigh(n,data) for n,data in graph.nodes.data()])))
        
    listNeigh= listNeigh=[l for l in listRawNeigh if  l !=""]
    
    listSubGraphs={}
    listNodes={}
    dfAll=pd.DataFrame()
    for l in listNeigh:
        subgraph,nodes=getSubgraphNeigh(graph, l,quiet)
        listSubGraphs[l]=subgraph
        listNodes[l]=nodes
        _, _, dfEdges= getSubgraphNeighWithEdgesAndFeat(graph,l,["grade"]) #lower caps
        dfEdges["Neighbourhood"] = [l for i in range(dfEdges.shape[0])]
        dfAll=pd.concat([dfAll,dfEdges ])

    return(listSubGraphs,listNodes,dfAll)


def getSubgraphListNeighOneGraph(graph, listNeigh, quiet=True):

    #Append to the list, don't use list comprehension
    listNodes = []
    for n in  listNeigh:
        listNodes += getListNodesNeigh(graph,n, quiet)

    subgraph = graphInduced=ox.utils.induce_subgraph(graph,listNodes)

    return (subgraph, listNodes)


def getSubgraphListNeigh(graph, listNeigh):
    
    listSubGraphs={}
    listNodes={}
    for l in listNeigh:
        subgraph,nodes=getSubgraphNeigh(graph, l)
        listSubGraphs[l]=subgraph
        listNodes[l]=nodes
        
    return(listSubGraphs,listNodes)



def getSubgraphListNeighQc(listNeigh=None):

    #Need to import this function explicitely because of ciruclar dependency => cant import all at the module level
    from OsmnxElev.OsmnxGraphCreation.osmnxCreateGraphElevGradesNeigh import loadGraphElevGradesNeighQc

    graphQc=loadGraphElevGradesNeighQc()

    if(listNeigh is None):
        shpQcCity = gpd.read_file(os.path.join(ROOT_DIR, "Data", "GeoData", "Neighbourhoods", "vdq-quartier.shp"))
        listNeigh = shpQcCity.NOM
    
    listSubgraphs,listNodes=getSubgraphListNeigh(graphQc, listNeigh)
    
    return(listSubgraphs,listNodes)


def getEdgesFromGraph(graph,featuresExtractListNames):
    

    listEdges=[(u,v) for u,v, dat in graph.edges.data() ]
    listFeatures={}
    
    dfEdges=pd.DataFrame({"Edge": listEdges})
    
    for l in featuresExtractListNames:
        try:
            #humm. nx.get_edge_attributes returns a dict where the keys are edges u,v and in the case of a multigraph u,v,w
            listFeatures[l] = [dat[l] for u,v,dat in graph.edges.data()]  #this really returns a list
        except Exception as e:
            print("Fatal error with feature ", l, " - ", str(e))
            listFeatures[l] = None
            
        dfEdges[l] = listFeatures[l]
    
    return(dfEdges)