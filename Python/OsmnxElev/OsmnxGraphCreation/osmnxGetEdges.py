
import osmnx as ox
import pandas as pd
import networkx as nx


def getDfAllEdges(graph):


    print("In getDfAllEdges => type of graph is " , type(graph))

    listNodePairs =[(u ,v) for u ,v, dat in graph.edges.data()]
    listStart =[l[0] for l in listNodePairs]
    listEnd =[l[1] for l in listNodePairs]

    #Make sure the edge grades have been computed
    if(len(nx.get_edge_attributes(G=graph,name="grade_abs"))==0):
        print("Computing absolute edge grade")
        graph = ox.add_edge_grades(graph, add_absolute=True)

    try:
        listGrades =[dat["grade_abs"] for u ,v ,dat in graph.edges.data()]
    except Exception as e:
        raise("Fatal error in getDfAllEdges: make sure grade_abs is present in the graph edges")


    dfEdges =pd.DataFrame({"startNode" :listStart,
                          "endNode": listEnd,
                          "startEndPair" :listNodePairs,
                          "grade_abs" :listGrades})

    return(dfEdges)