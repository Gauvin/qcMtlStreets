
import numpy as np
import pandas as pd


def getBboxFromList(bboxList):
    matBbox=np.array(bboxList).reshape(2,2).transpose()
    
    dict2={}
    dict2["key"] = ["min","max"]
    dict2["lng"] = matBbox[0,:]
    dict2["lat"] = matBbox[1,:]
    dfBbox=pd.DataFrame.from_dict(dict2)
    
    return(dfBbox)



def get4by4Bbox(dfBbox):
    
 
    
    #Create the 4 points of the bbox
    dfBbox["dummy"] = np.ones(dfBbox.shape[0])

    dfMerged=pd.merge(dfBbox,dfBbox,
        on="dummy")
    
    #Remove the dummy column
    dfMerged.drop(columns={"dummy"},inplace=True)

    
    return(dfMerged)



def checkWithinInterval(x, xmin, xmax):
    return( (x <= xmax) & (x >= xmin))


#Make sure within the bouding box
def checkBoxRow(i, dfPoints, dfBox):
    
    try:
        isInBbox=        checkWithinInterval( dfPoints.loc[i, "lng"] , dfBox.loc [0,"lng_x"] ,  dfBox.loc [3,"lng_y"] ) & \
        checkWithinInterval( dfPoints.loc[i, "lat"]   , dfBox.loc [0,"lat_x"] ,  dfBox.loc [3,"lat_y"] ) 
    except Exception as e:
        raise Exception("Fatal error at row " + str(i) + " " + str(e))
    
    return( isInBbox)