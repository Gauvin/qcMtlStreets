
import numpy as np
import pandas as pd
import itertools
import networkx as nx

from OsmnxElev.OsmnxCuts import *
 



def getIdxLowerGraph(i,j):
    
    if i < j:
        return(i,j)
    else:
        return(j,i)
    

def getAdjMatrixFromGraph(graph,
                        weightCol): #If None, then each edge has weight 1. (from  nx doc - https://networkx.github.io/documentation/networkx-1.9/reference/generated/networkx.linalg.graphmatrix.adjacency_matrix.html)
    adjMatrix  = nx.adjacency_matrix(graph, weight=weightCol)
    adjMatrix = adjMatrix.todense()

    if( np.sum( np.sum( ~np.isclose(adjMatrix,adjMatrix.T) ) ) ):
        print("Warning in getAdjMatrixFromGraph: adj matrix is not symmetric")

    return(adjMatrix)

def getAdjMatrixFromEdgeList(dfEdgeListIn,
                             nodeNameIn="startNeigh",
                             nodeNameOut="endNeigh",
                             useDirected=False,
                             weightCol=None,
                             aggFun="sum"):

    #Don't touch the edges df inplace
    dfEdgeList=dfEdgeListIn.copy()
    
    #Check column existence (node name)
    if(np.isin( [nodeNameIn,nodeNameOut], dfEdgeList.columns ).all() == False):
        raise Exception("Fatal error in getAdjMatrixFromEdgeList! Columns ", nodeNameIn , " and ", nodeNameOut, " not present in the df")
    
    #Determine if the graph is weighted or not
    if(weightCol is None):
        print("No weight column input: considering an unweighted graph")
        weightCol="count"
        dfEdgeList[weightCol] = [1 for i in range(dfEdgeList.shape[0])] #just add a 1 for each of the edges

    #Add in missing edges to get a nXn matrix where n is the number of nodes (if not, then we will get a sparser matrix)
    dfEdgeList=addMissingEdges(dfEdgeList,
                               True,
                               nodeNameIn,
                               nodeNameOut,
                               weightCol) #always need to add all permutations (regardless of wether the graph is directed or not)
        
    #Determime duplicate edge existence: if so then remove them by tallying/summing the duplicated edge weights
    if(dfEdgeList[[nodeNameIn,nodeNameOut]].duplicated().any()==True):
        dfEdgeList=removeDupAggEdges(dfEdgeList,nodeNameIn,nodeNameOut,weightCol,useDirected,aggFun)
    
    #Now pivot the resulting matrix and given the row: nodeNameOut=i and nodeNameOut=j and weightCol=k 
    #dfAdjMatrix_ij = k
    dfAdjMatrix=dfEdgeList[[nodeNameIn,nodeNameOut,weightCol]].pivot(index=nodeNameIn,columns=nodeNameOut)

    #Make sure the matrix is symmetric (not sure this is required)
    if(useDirected==False ):
        dfAdjMatrix=makeSquareDfSymmetric(dfAdjMatrix)

    
    return(dfAdjMatrix)
    
    
def addMissingEdges(dfEdges,
                   useDirected=False,
                   nodeNameIn="startNeigh",
                   nodeNameOut="endNeigh",
                    weightCol="count"):
    
    #Get the list of all neigh (important to take the union of rows and columns)
    listNodes= np.unique( np.concatenate( [dfEdges[nodeNameIn], dfEdges[nodeNameOut]]) )

    #Need to add the index

    if(useDirected==True):
        print("in addMissingComb => adding all missing permutations (directed graph)")
        #permutations
        for p in itertools.permutations(listNodes,2) :
            listPerm=[ str(i)+str(j) for i,j in dfEdges[[nodeNameIn,nodeNameOut]].values ]
            if( np.isin("".join(p), listPerm ).all() == False ): #watchout with np.isin and np.unique and tuples (i,j)
                #dfRow=pd.DataFrame( [ [(np.nan,np.nan), 0 , np.nan, np.nan, p[0], p[1]] ] , columns=dfEdges.columns.tolist()) #assign a weight of 0
                dfRow=createNaRowWithColsNames(dfEdges)
                dfRow[nodeNameIn] =  p[0]
                dfRow[nodeNameOut] =  p[1]
                dfRow[weightCol] = 0
                dfEdges=dfEdges.append(dfRow)
    else:
        print("in addMissingComb => adding all missing combinations (undirected graph)")
        #combinations
        for p in itertools.combinations(listNodes,2) :
            listCombo=[ str(min(i,j))+str(max(i,j)) for i,j in dfEdges[[nodeNameIn,nodeNameOut]].values ]
            if( np.isin("".join(p), listCombo ).all() == False ):
                dfRow=createNaRowWithColsNames(dfEdges)
                dfRow[nodeNameIn] =  p[0]
                dfRow[nodeNameOut] =  p[1]
                dfRow[weightCol] = 0
                dfEdges=dfEdges.append(dfRow)

    return(dfEdges)
    
    
def createNaRowWithColsNames(dfToCopy):
    dfToReturn=pd.DataFrame( np.ones(dfToCopy.shape[1])* np.nan )
    dfToReturn=dfToReturn.T
    dfToReturn.columns=dfToCopy.columns
    
    return(dfToReturn)


def makeSquareMatrixSymmetric(mat):
    
    if(mat.shape[0]!= mat.shape[1]):
        raise Exception("Fatal error in makeSquareMatrixSymmetric => the matrix is not symmetric")
    
    #Need to convert the Nan to 0 if we want to avoid having a symmetric matrix of all nas
    matSymm=np.nan_to_num( np.transpose(mat)) + np.nan_to_num(mat)
    
    if( np.allclose(matSymm, matSymm.T, rtol=10**-4,atol=10**-4) == False) :
        raise ("Fatal error in makeSquareMatrixSymmetric => matrix does not seem symmetric")
    
    return( matSymm )



def makeSquareDfSymmetric(df):

    #Make sure the indexed match after the group by
    newIndex=df.columns.droplevel()
    df.columns = newIndex
    df.index = newIndex

    if(df.shape[0]!= df.shape[1]):
        raise Exception("Fatal error in makeSquareDfSymmetric => the matrix is not symmetric")

    if(np.allclose(df.fillna(0), df.fillna(0).T, atol=10**-4)):
        print("Matrix already square")
        dfSquare=df

    else:

        #Need to convert the Nan to 0 if we want to avoid having a symmetric matrix of all nas
        dfSquare= df.transpose().fillna(0) + df.fillna(0)

        if( np.allclose(dfSquare, dfSquare.T, rtol=10**-4 ) == False) :
            raise ("Fatal error in makeSquareDfSymmetric => df does not seem symmetric")
    
    return( dfSquare )
    
def removeDupAggEdges(dfEdgesWithDup,
                      nodeNameIn="startNeigh",
                      nodeNameOut="endtNeigh",
                      weightCol="count",
                      useDirected=False,
                      aggFct="sum"):

    if(useDirected==False):
        dfEdgesWithDup['indexKey']= [getIdxLowerGraph(idxx, idxy) for \
                                     idxx, idxy in dfEdgesWithDup[["startIdx", "endIdx"]].values ]
    else:
        dfEdgesWithDup['indexKey'] = [(idxx, idxy) for \
                                      idxx, idxy in dfEdgesWithDup[["startIdx", "endIdx"]].values ]

    #If edge (a,b) is duplicated on x rows i1 ... ix
    #then simply take the total count: S=sum_{k=1}^x count_{ik} 
    #if weighted, this will be the sum of weights
    #otherwise, S is simply the number of repeated rows (x)
    #dfEdgesNoDup=dfEdgesWithDup.groupby([nodeNameIn,nodeNameOut]).agg( {weightCol: "sum"}).reset_index()  #watrch out, seems like rows with nas are removed
    print("in removeDupAggEdges, using ", aggFct, " as the aggregation function")
    dfEdgesNoDup = dfEdgesWithDup.groupby([nodeNameIn,nodeNameOut,"indexKey"]).agg(
        {weightCol: aggFct}).reset_index()  # watch out, seems like rows with nas are removed

    if(useDirected==False):
        #Get the symmetric part if undirected graph
        dfEdgesNoDupUndirected= dfEdgesWithDup.groupby(["indexKey"]).agg(
            {weightCol: aggFct }).reset_index()

        dfEdgesNoDup=pd.merge(dfEdgesNoDup, dfEdgesNoDupUndirected, how='left', on="indexKey" )

        dfEdgesNoDup=dfEdgesNoDup.drop(columns=[weightCol+"_x"]).rename(columns={weightCol+"_y": weightCol})



    return(dfEdgesNoDup)