import re
import numpy as np
import pandas as pd
import geopandas as gpd
import os
import osmnx as ox
import pickle
import networkx as nx

from shapely.geometry import Point
from shapely.geometry import Polygon, MultiPolygon
from descartes import PolygonPatch
from matplotlib.collections import PatchCollection

from OsmnxElev.definitions import *

from OsmnxElev.OsmnxGraphCreation.osmnxGetGraphInducedNeigh import *
from OsmnxElev.OsmnxGraphCreation.osmnxCreateGraphElevGradesNeigh import *
from OsmnxElev.OsmnxGraphCreation.osmnxBbox import *


def isPointInPolygon(point,poly):
    return( poly.contains( point ) )
           
def isPointInGroupPolygons(point,listPoly):
    return( np.any( [ isPointInPolygon(point,poly) for poly in listPoly ] ) ) 

def getIdxPointInPolygon(listPoints,poly):
    return(  [ isPointInPolygon(point,poly) for point in listPoints ] ) 

def getIdxListPointsInGroupPolygons(listPoints,listPoly):
    return( [ isPointInGroupPolygons(point, listPoly ) for point in listPoints] )



def getPointNeighbourhoodAdjList(listPoints, dfNeigh, varNameID="NOM"):

    dfPointsPolyAdjList=pd.DataFrame()

    #With iterrows, no possible bug related to faulty indexing
    #For instance if we subseted dfNeigh, but did not reset index, then we might get some errors if we try to idx from 0 to n-1
    for k,row in dfNeigh.iterrows():
        name=row[varNameID]
        dfPointsPolyAdjList[name]= getIdxPointInPolygon(listPoints, row["geometry"] )
        
    return(dfPointsPolyAdjList)


def getLngLatNodesInGraph(graph):

    listIndices = [dat[0] for dat in graph.nodes.data()]

    listYLat = [dat[1]["y"] for dat in graph.nodes.data()]
    listXLng = [dat[1]["x"] for dat in graph.nodes.data()]

    #Create a simple feature column of Points
    listPointsLngLat = [Point(lng, lat) for lat, lng in zip(listYLat, listXLng)]

    #Create the spatial geopandas df
    dfPoints = gpd.GeoDataFrame(geometry=listPointsLngLat)  # use lng, lat
    dfPoints['lng'] = listXLng
    dfPoints['lat'] = listYLat
    dfPoints["nodeIndex"] = listIndices

    return(dfPoints)


def getPointIndexInNeigh(dfPointsPolyAdjList, dfPointsSubsetInPoly, neigh):

    dfPointsSubsetInPoly.reset_index(inplace=True,drop=True)
    dfPointsPolyAdjList.reset_index(inplace=True,drop=True)

    return( dfPointsSubsetInPoly.loc[ dfPointsPolyAdjList[neigh], "nodeIndex"].values )



def setPointNeighbourhood(graph, listNodes, neigh):
    listNodeIndicesToRet=[]
    for k,dat in graph.nodes(data=True):
        try:
            if np.isin( k,listNodes):
                dat["Neighbourhood"] = neigh  #ok this will indeed modify the value
                listNodeIndicesToRet.append(k)
        except Exception as e:
            print("Fatal error in setPointNeighbourhood -- at neigh ", neigh, " node index: ", k)

    #Actually update the graph attribute (pass in a dictionnary of dictionnaries)
    #nx.set_node_attributes(graph, {i : { "Neighbourhood" : neigh } for i in listNodeIndicesToRet}) #not required - use for tests
    print("Here is a subset of the neighbourhoods : ", \
          ", ".join(set(list(nx.get_node_attributes(graph, "Neighbourhood").values()))))

    return(listNodeIndicesToRet)



def setPointAllNeighbourhood(graph, dfPointsSubsetInPoly, dfNeigh, varnameID):
    
    dfPointsPolyAdjList=getPointNeighbourhoodAdjList(dfPointsSubsetInPoly["geometry"], dfNeigh, varnameID)
    dictListNodes={}
        
    for k,row in dfNeigh.iterrows():
        try:
            neigh= row[varnameID]
            listNodes=  getPointIndexInNeigh(dfPointsPolyAdjList,dfPointsSubsetInPoly, neigh)
            listRet=setPointNeighbourhood(graph, listNodes, neigh)
            dictListNodes[neigh] = listRet
        except Exception as e:
            print("Fatal error in setPointAllNeighbourhood -- at neigh ", neigh, " -- " , str(e))
            
    return(dictListNodes)

def getInducedSubgraphNeigh(graph, shp, varNameID, listNeigh):

    # Select only the desired neighbourhoods
    if listNeigh is not None:
        shpNeighFiltered = shp[np.isin(shp[varNameID], listNeigh)]
    else:
        shpNeighFiltered = shp


    dfPointsSubsetInPoly = getPointsInPoly(graph,shpNeighFiltered, listNeigh)


    graphQcinduced = ox.utils.induce_subgraph(graph,
                                              dfPointsSubsetInPoly["nodeIndex"].values)

    return(graphQcinduced, shpNeighFiltered, dfPointsSubsetInPoly)


def getListNeighGraph(graph, shpNeigh, listNeigh,varNameID="NOM"):
    
    listGraphs=[]
   

    #Get the induced subgraph
    graphQcinduced, shpNeighFiltered, dfPointsSubsetInPoly=getInducedSubgraphNeigh(graph,
                                                                shpNeigh,
                                                                varNameID,
                                                                listNeigh)
        
    #Add the neighbourhood to all the nodes + retrieve a dict of neighbourhoods and nodes
    dictListNodes=setPointAllNeighbourhood(graphQcinduced,
                                           dfPointsSubsetInPoly,
                                           shpNeighFiltered ,
                                           varNameID)
    
    #Loop through all neighbourhoods and create the induced subgraph
    for k,v in dictListNodes.items():
        name=re.sub("[/-]","_",k)
        fileNameWithPath = os.path.join(ROOT_DIR,"Data","Pickle","graphStreetsByNeigh", name + ".pickle")
        
        if os.path.isfile(fileNameWithPath) == False:
            print(fileNameWithPath + " does not exist => creating the induced subgraph")
            inducedSubgraph=ox.utils.induce_subgraph(graph,v)
            
            with open(fileNameWithPath,'wb') as fileHandle:
                pickle.dump(file=fileHandle,
                       obj=inducedSubgraph)
        else:
             with open(fileNameWithPath,'rb') as fileHandle:
                inducedSubgraph=pickle.load(fileHandle)
                
        listGraphs.append(inducedSubgraph)
        
    return(listGraphs)


def getPointsInPoly(graph, shp, listNeigh):


    # Get the lng lat of nodes in the graph
    dfPoints = getLngLatNodesInGraph(graph)

    # Get the bounding box over the neighbourhoods
    dfBbox = getBboxFromList(shp.total_bounds)
    dfBbox4points = get4by4Bbox(dfBbox)

    # Only select the points that fall within the neighbourhoods
    idxInBbox = [checkBoxRow(i, dfPoints, dfBbox4points) for i in range(dfPoints.shape[0])]
    print("There are ", np.sum(idxInBbox) / dfPoints.shape[0],
          " of points in the bounding box in neighbourhoods " + ", ".join(
              listNeigh) if listNeigh is not None else "all neighbourhoods")

    dfPointsSubset = dfPoints[idxInBbox]
    dfPointsSubset.reset_index(drop=True, inplace=True)

    idxSubsetInPoly = getIdxListPointsInGroupPolygons(dfPointsSubset["geometry"],
                                                      shp["geometry"])

    dfPointsSubsetInPoly = dfPointsSubset.loc[idxSubsetInPoly]

    return(dfPointsSubsetInPoly)





def getInducedSubgraphNeighQc(listNeigh):

    #Need to import this function explicitely because of ciruclar dependency
    from OsmnxElev.OsmnxGraphCreation.osmnxCreateGraphElevGradesNeigh import loadGraphElevGradesNeighQc

    graphQc = loadGraphElevGradesNeighQc()

    # Get the shp file
    shpQcCity = gpd.read_file(os.path.join(ROOT_DIR, "Data", "GeoData", "Neighbourhoods", "vdq-quartier.shp"))

    graphQcinduced, shpQcNeighFiltered, dfPointsSubsetQcNeigh = getInducedSubgraphNeigh(graphQc,
                                                                                shpQcCity,
                                                                                "NOM",
                                                                                listNeigh)


    return(graphQcinduced, shpQcNeighFiltered, dfPointsSubsetQcNeigh)


def getListNeighGraphsQc(listNeigh=[ "Saint-Jean-Baptiste", \
                                     "Vieux-Québec/Cap-Blanc/Colline parlementaire",  \
                                     "Saint-Roch"]):
    
    place=['Québec city, canada']

    # osmnx graph
    graphQc = ox.graph_from_place(place,
                                  simplify=False,
                                  retain_all=True)

    # neighbourhood shape file
    shpQcCity = gpd.read_file(os.path.join(ROOT_DIR, "Data", "GeoData", "Neighbourhoods", "vdq-quartier.shp"))

    # Create an induced subgraph for each neighbourhood
    listGraphs = getListNeighGraph(graphQc,
                                   shpQcCity,
                                   listNeigh)

    return (listGraphs)




def getDfAllNodes(graph):


    #Get the initial dataframe
    listNodes =[i for i, dat in graph.nodes.data()]
    dfNodes = pd.DataFrame({"node": listNodes})

    #Make sure the elevations have been computed
    if(len(nx.get_node_attributes(G=graph,name="elevation"))> 0):
        dfNodes['elevation'] = [dat["elevation"] for u, dat in graph.nodes.data()]
    else:
        print("In getDfAllNodes: cannot use node elevation")

    #Make sure the Neighbourhood have been computed
    if(len(nx.get_node_attributes(G=graph,name="Neighbourhood"))> 0):
        dfNodes['Neighbourhood'] = [dat["Neighbourhood"] for u, dat in graph.nodes.data()]
    else:
        print("In getDfAllNodes: cannot use node Neighbourhood")


    return(dfNodes)

    

