    
import osmnx as ox
import numpy as np
import matplotlib.pyplot as plt
import geopandas as gpd
import os

from OsmnxElev.OsmnxPlots import *
from OsmnxElev.OsmnxGraphCreation import *
from OsmnxElev.OsmnxPlots.osmnxColNeigh import *

from matplotlib import cm
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.patches as mpatches

def plotGraphGradeNeighQc(listNeigh=["Saint-Jean-Baptiste", 
                                     "Saint-Roch", 
                                     "Vieux-Québec/Cap-Blanc/Colline parlementaire",
                                     "Montcalm",
                                     "Saint-Sauveur"],
                          legPosParam="best"):

    #Get the graph
    graphQc=loadGraphElevGradesNeighQc()
 
    #Get the shp file 
    shpQcCity = gpd.read_file(os.path.join(ROOT_DIR, "Data","GeoData","Neighbourhoods","vdq-quartier.shp"))
    
    #Get the induced subgraph 
    graphQc3Neigh, shpQc3Neigh, dfPointsSubsetQc3Neigh= getInducedSubgraphNeigh(graphQc, 
                                                                                shpQcCity, 
                                                                                "NOM",
                                                                                listNeigh)
    
    #Implement dictCol
    dictCol=getDefaultDictColQc()
    
    #Plot the graph + return the figure
    return( plotGraphGradeNeigh(graphQc3Neigh, 
                                shpQc3Neigh,
                                listNeigh,
                                dictCol,
                                absoluteVal=True,
                                strName="\nQuebec City " + ",".join(listNeigh),
                                legPos=legPosParam,
                                neighColName = "NOM")
          )


def plotGraphGradeNeighMtl(listNeigh=["Côte-des-Neiges",
                                      "Outremont",
                                      "Le Plateau-Mont-Royal"],
                           legPosParam="best"):
    # Get the graph
    graphMtl = loadGraphElevGradesNeighMtl()

    # Get the shp file
    shpMtl = gpd.read_file( os.path.join(ROOT_DIR, "Data", "GeoData", "MontrealNeighbourhoods", "Quartiers_sociologiques_2014.shp"))

    # Get the induced subgraph
    graphMtl3Neigh, shpMtl3Neigh, dfPointsSubsetMtl3Neigh = getInducedSubgraphNeigh(graphMtl,
                                                                                 shpMtl,
                                                                                 "Q_socio",
                                                                                 listNeigh)

    #Fuck it, no need for a custom default color dict
    #Just assign one of shpMtl.shape[0]-1 colors to each neighbourhood from a diverging color palette
    colsDiverging = get36DivergingColors()
    shpMtl['col'] = colsDiverging[0:shpMtl.shape[0]]
    dictCol = {row["Q_socio"]: row['col'] for i, row in shpMtl.iterrows()}


    # Plot the graph + return the figure
    return (plotGraphGradeNeigh(graphMtl3Neigh,
                                shpMtl3Neigh,
                                listNeigh,
                                dictCol,
                                absoluteVal=True,
                                strName="\nMontreal " + ",".join(listNeigh),
                                legPos=legPosParam,
                                neighColName="Q_socio"
                                )
            )


def plotGraphGradeNeigh(graph, shp, listNeigh,dictCol, neighColName="NOM", absoluteVal=True,strName="", legPos="best"):

    
    
    #Get the colors
    if(absoluteVal):
        print("Using absolute grades")
        listGradeEdges=[  abs(data["grade"]) for u,v,data in graph.edges(data=True) ]
        listEdgeColors3neigh=getColGraphAttrEdges(graph, listGradeEdges, "grade_abs", numQuantiles=20)                                   
    else:
        print("Using raw grades")
        listGradeEdges=[  (data["grade"]) for u,v,data in graph.edges(data=True) ]
        listEdgeColors3neigh=getColGraphAttrEdges(graph, listGradeEdges, "grade", numQuantiles=20)               
                                                 
    fig,ax=ox.plot_graph(graph,
                         fig_height=12, 
                         fig_width=12,
                         edge_color=listEdgeColors3neigh,
                         node_size=0,
                         show=False,
                         close=False)


    listLegendHandles=[]
    for k,row in shp.iterrows():
        if( np.isin(row[neighColName] ,  listNeigh) ):
            patch=PolygonPatch(row.geometry, 
                               facecolor="white", 
                               edgecolor=dictCol[row[neighColName]],
                               linewidth=8,
                               alpha=0.2,
                               zorder=-1)
            ax.add_patch(patch )
            listLegendHandles.append(mpatches.Patch(color=dictCol[row[neighColName]],  label=row[neighColName]))

    fig.suptitle("Streets with edge grades by neighbourhood " + strName) 
    ax.legend(title="Neighbourhood",
               handles=listLegendHandles,
               loc=legPos)

    fig.savefig(os.path.join(ROOT_DIR, "Figures", "EdgeGrades", "GraphWithEdgesAndNeighbourhoods" + strName + ".png"))
    
    return(fig,ax)





def plotGraphGrade(graph, absoluteVal=True, numQuantiles=20):
    
   
    #Get the colors
    if(absoluteVal):
        print("Using absolute grades")
        listGradeEdges=[  abs(data["grade"]) for u,v,data in graph.edges(data=True) ]
        listEdgeColors3neigh=getColGraphAttrEdges(graph, listGradeEdges, "grade_abs", numQuantiles=20)                                   
    else:
        print("Using raw grades")
        listGradeEdges=[  (data["grade"]) for u,v,data in graph.edges(data=True) ]
        listEdgeColors3neigh=getColGraphAttrEdges(graph, listGradeEdges, "grade", numQuantiles=20)               
                                                 
    fig,ax=ox.plot_graph(graph,
                         fig_height=12, 
                         fig_width=12,
                         edge_color=listEdgeColors3neigh,
                         node_size=0,
                         show=False,
                         close=False)


    return(fig,ax)