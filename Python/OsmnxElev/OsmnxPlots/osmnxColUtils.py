import matplotlib
from matplotlib import cm
import matplotlib.pyplot as plt
import matplotlib.colors as colors

import osmnx as ox

import pandas as pd
import numpy as np
import bisect
import os

from OsmnxElev import *


def getColorCategorical(listVals,
                        groupName="group",
                        colName="col"):

    scalarMap = cm.ScalarMappable(cmap=cm.Set1)
    uniqueVals = np.unique(listVals)


    # define the colormap
    cmapDiscrete = cm.Set1

    if(len(uniqueVals) >  cmapDiscrete.N):
        print("warning, num colors required in getColorCategorical larger than the Set1 size => cycling")

    #Extract all colors and cycle if necessary with modulo
    uniqueCols = [cmapDiscrete(i % cmapDiscrete.N) for i in range(len(uniqueVals))]

    dfCol=pd.DataFrame({groupName: uniqueVals,
                        colName: uniqueCols})

    return(dfCol)

#For compatibility
def getColorFromElev(bins,colList, elev):
    return(getColorFromAttr(bins,colList, elev))


#Main fct
def getColorFromAttr(bins,colList, attr):
    try:
        idx=bisect.bisect_left(bins, attr)  #watch out with the indexing, bisect.bisect_left(bins, attr) returns values in [1,20] 
        c=colList[idx-1]
    except Exception as e:
        print("Fatal error at attribute ", attr, " idx ", idx)
        
    return(c)
    
    
def getColorBinsQuantiles(listVals, numQuantiles=20):
 
    #Set the color map
    cNorm = matplotlib.colors.Normalize(vmin=1, vmax=numQuantiles)
    scalarMap = cm.ScalarMappable(norm=cNorm, cmap=cm.plasma)

    #Get the quantiles
    series, bins = pd.qcut(x=listVals,
                            q=numQuantiles, 
                            labels=np.linspace(1,numQuantiles,numQuantiles), 
                            retbins=True)

    colList = [scalarMap.to_rgba(i) for i in range(numQuantiles)]

    return(series,bins,colList)