
import osmnx as ox
import numpy as np
import matplotlib.pyplot as plt
import geopandas as gpd
import pandas as pd
import os

from OsmnxElev import *
from OsmnxElev.OsmnxGraphCreation import *
from OsmnxElev.OsmnxPlots import *



def plotNeighGraphQc(listNeigh, simplifyGraph=True):

    '''

    Shitty convenience function to plot the nodes in an induced subgraph according to their neighbourhoods

    Uses getColGraphAttrNodesCateg which maps neighbouhoods to diverging color palette to get a df of colors, which is converted to a dict.
    A list of colors is then created by iterating over graph.nodes.data() and is then passed on to osmnx.plot_graph()


    :param listNeigh: list of strings
    :param simplifyGraph: boolean
    :return: fig, ax : matplotlib.pyplot,
    '''


    #Get the induced subgraph on neighbourhoods
    graphQc, shpQc, dfPointsSubsetQcc = getInducedSubgraphNeighQc(listNeigh)

    #Simplify if  required
    if(simplifyGraph):
        print("In plotNeighGraphQc => simplifying graph")
        graphQc = ox.simplify_graph(graphQc)

    # Get the neighbourhoods
    listNodeNeigh = [(dat["Neighbourhood"]) for u, dat in graphQc.nodes.data()]

    #Get the df mapping neighbourhood names to colors
    dfColorsNodeNeigh = getColGraphAttrNodesCateg(listNodeNeigh)

    #Convert to dictionnary

    #Watch out,the following approach with pandas Series only works if we use .col.values - other wise with .col, the col column is all nan
    #dictColorsNodeNeigh  = pd.Series(dfColorsNodeNeigh.col.values,
    #                            index=dfColorsNodeNeigh.nodeGroup).to_dict()
    dictColorsNodeNeigh = {row["nodeGroup"]: row['col'] for i,row in dfColorsNodeNeigh.iterrows()}

    #Get the colors of the nodes in the graph in the appropriate order
    listCols = [dictColorsNodeNeigh[dat["Neighbourhood"]] for n, dat in graphQc.nodes.data()]

    #Finally, plot the graph
    fig, ax = ox.plot_graph(graphQc,
                            node_color=listCols)


    return (fig, ax)