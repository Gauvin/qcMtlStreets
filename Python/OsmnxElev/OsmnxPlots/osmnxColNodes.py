
import matplotlib
from matplotlib import cm
import matplotlib.pyplot as plt
import matplotlib.colors as colors

import osmnx as ox

import pandas as pd
import numpy as np
import bisect
import os

from OsmnxElev import *
from OsmnxElev.OsmnxPlots import *
 
def getColGraphAttrNodes(graph, listAttrValues, nameAttrStr, numQuantiles=20):
    
    #Make sure the attribute exists in the graph
    if(len(nx.get_node_attributes(graph,name=nameAttrStr))==0):
        raise("Fatal error in plotGraphAttrNodes: attribute", nameAttrStr, " not found in the graph")
        
        
    #Get the quantiles and colors
    series, bins ,colList = getColorBinsQuantiles(listAttrValues,numQuantiles)

    #Get the colors
    listNodeColors=[  getColorFromAttr(bins, colList, data[nameAttrStr]) for n,data in graph.nodes(data=True) ]
    
    #Validation
    if(len(listColors)==0 & len(graph.nodes()) > 0):
        raise Exception("Fatal error in getColGraphAttrNodes with attribute " , nameAttrStr, " =>  no nodes with this attribute present in the graph")
    
    return(listNodeColors)



def getColGraphAttrNodesCateg(nodeVals):

    dfNodes = pd.DataFrame(nodeVals).reset_index().groupby(0)["index"].nunique()

    dfNodes = pd.DataFrame(dfNodes).reset_index().rename(columns={0: "nodeGroup",
                                                                  "index": "count"})

    dfCol=getColorCategorical(nodeVals,
                              groupName="nodeGroup",
                              colName="col")

    dfColWithCount = pd.merge(dfCol, dfNodes, how="left")

    return(dfColWithCount)