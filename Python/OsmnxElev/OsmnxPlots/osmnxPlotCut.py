
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from matplotlib.collections import PatchCollection
from matplotlib import cm
from descartes import PolygonPatch

import osmnx as ox

import geopandas as gdp
import pandas as pd
import numpy as np
import os

from OsmnxElev.OsmnxGraphCreation import *
from OsmnxElev.OsmnxCuts import *
from OsmnxElev.OsmnxPlots import *
from OsmnxElev.definitions import *

'''
Plot the neighbourhood polygons (as descartes PolygonPatch ) + edges between them (cuts) in red

'''
def plotNeighCutQc(listNeigh=["Saint-Jean-Baptiste",\
                              "Saint-Roch",\
                              "Vieux-Québec/Cap-Blanc/Colline parlementaire",\
                              "Montcalm",\
                              "Saint-Sauveur"]):
    
    #osmnx graph with neigh
    graphQc=loadGraphElevGradesNeighQc()

    graphQcInduced,listNodes=getSubgraphListNeighOneGraph(graphQc,listNeigh)
    
    #Shp file
    shpQcCity = gpd.read_file(os.path.join(ROOT_DIR, "Data", "GeoData", "Neighbourhoods", "vdq-quartier.shp"))
    shpNeighFiltered=shpQcCity[np.isin(shpQcCity["NOM"], listNeigh)]
    
    #Get all edges formed by the nC2 cuts
    dfEdgesDirected=getAllListEdgesDirectedMultigraph(graphQcInduced, shpNeighFiltered.NOM.unique())
    
    
    listAllPairs=[ (row["startNode"],row["endNode"]) for k, row in dfEdgesDirected.iterrows()]
    ec=[ getEdgeColorCut(u,v,listAllPairs) for u,v,dat in  graphQcInduced.edges.data()  ]
    es=[ getEdgeSizeCut(u,v,listAllPairs) for u,v,dat in  graphQcInduced.edges.data()  ]
    
    fig, ax = ox.plot_graph(graphQcInduced,
             edge_color=ec,
             edge_linewidth=es,
             node_size=[0 for i in graphQcInduced.nodes.data()],
             show=False,
             close=False           
                )
    plt.close()
    
    for idx, row in shpNeighFiltered.iterrows():
        patch=PolygonPatch( row["geometry"], alpha=0.1, zorder=-1)
        ax.add_patch(patch)
    
    
    margin=0.2
    west, south, east, north =shpNeighFiltered.unary_union.bounds
    margin_ns = (north - south) * margin
    margin_ew = (east - west) * margin
    ax.set_ylim((south - margin_ns, north + margin_ns))
    ax.set_xlim((west - margin_ew, east + margin_ew))
    
    return(fig,listAllPairs,dfEdgesDirected)