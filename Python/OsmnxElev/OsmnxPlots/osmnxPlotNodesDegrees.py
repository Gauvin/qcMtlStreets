
import networkx as nx
import osmnx as ox

from OsmnxElev import *



def plotNodeDegrees(graph, numQuantiles=20):
    
    #Determine if the degrees are present
    #If not compute them
    if(len(nx.get_edge_attributes(graph,name="degree"))==0):
        
        dictNodesDegrees=dict(nx.degree(graph))
        
        for n, dat in graph.nodes.data():
            if n in dictNodesDegrees.keys():
                dat["degree"] = dictNodesDegrees[n]
            else:
                raise Exception("Fatal error, node ", n , " could not find degree")
    
    #Get the grades
    listDegrees=[ (dat["degree"]) for u,v,dat in graph.nodes.data() ]

    #Get the colors
    listColorsNodeDegrees=getColGraphAttrNodes(graph, listDegrees, "degree" , numQuantiles)
    
    #Plot the graph
    fig,ax=ox.plot_graph( graph,  
                         node_color=listColorsNodeDegrees)
    
    return(fig,ax)




def plotNodeDegreesQc( numQuantiles=20):
    
    graphQc=loadGraphElevGradesNeighQc()
    
    return(plotNodeDegrees(graphQc,numQuantiles))
    
 