import matplotlib
from matplotlib import cm
import matplotlib.pyplot as plt
import matplotlib.colors as colors

import osmnx as ox
import networkx as nx

import pandas as pd
import numpy as np
import bisect
import os

from OsmnxElev import *
from OsmnxElev.OsmnxPlots import *

def getColGraphAttrEdges(graph, listAttrValues, nameAttrStr, numQuantiles=20):
    
    #Get the quantiles + col map
    series, bins, colList = getColorBinsQuantiles(listAttrValues, numQuantiles)
    
    #Make sure the attribute exists in the graph
    if(len(nx.get_edge_attributes(graph,name=nameAttrStr))==0):
        raise("Fatal error in plotGraphAttrEdges: attribute", nameAttrStr, " not found in the graph")
    
    #Get the colors
    listEdgeColors=[  getColorFromAttr(bins, colList, data[nameAttrStr]) for u,v,data in graph.edges(data=True) ]

    #Validation
    if(len(listEdgeColors)==0 & len(graph.edges()) > 0):
        raise Exception("Fatal error in plotGraphAttrEdges with attribute " , nameAttrStr, " =>  no edges with this attribute present in the graph")
    
    return(listEdgeColors )

