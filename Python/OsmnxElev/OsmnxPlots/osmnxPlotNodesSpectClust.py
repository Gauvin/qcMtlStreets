
import osmnx as ox

from OsmnxElev.OsmnxPlots.osmnxColNodes import *
from OsmnxElev.OsmnxCuts.osmnxNormCut import *

def plotGraphSpecClustNeighQc(listNeigh=["Saint-Jean-Baptiste",
                                     "Saint-Roch",
                                     "Vieux-Québec/Cap-Blanc/Colline parlementaire",
                                     "Montcalm",
                                     "Saint-Sauveur"],
                              weightCol=None,
                              numClust=None,
                              simplifyGraph=False):

    numClust= len(listNeigh) if numClust is None else numClust

    #Get the node assignment
    graphCC, adjMat, nodeAssign=computeSpectralClustFromGraphQc(listNeigh,weightCol,numClust,simplifyGraph)

    #Get the node color
    dfNodeCol= getColGraphAttrNodesCateg(nodeAssign)

    dfAllNodes=pd.DataFrame({"nodeGroup": nodeAssign})

    dfAllNodesCol= pd.merge( dfAllNodes, dfNodeCol , how="left", on="nodeGroup")

    #Plot the graph
    fig,ax = ox.plot_graph(graphCC,
                  node_color=dfAllNodesCol.col)

    return(fig,ax)