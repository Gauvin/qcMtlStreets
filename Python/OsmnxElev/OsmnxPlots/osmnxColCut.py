


        

def getEdgeColorCut(u,v,cutList):
    for i in range(len(cutList)):
        if( (u==cutList[i][0]) & (v ==cutList[i][1])):
            col="red"
            break
        else:
            col="grey"
        
    return(col)


def getEdgeSizeCut(u,v,cutList,defaultCol="grey"):
    if(getEdgeColorCut(u,v,cutList)==defaultCol):
        size=0
    else:
        size=2
        
    return(size)

