
import geopandas as gpd
import numpy as np
import os
import pandas as pd


import matplotlib.pyplot as plt

from OsmnxElev.definitions import *


def get36DivergingColors():
    
    listColMapsNames=["Set1","Set2","Set3","Dark2"]
    
    listColMaps=[  plt.cm.get_cmap(n) for n  in listColMapsNames ]
 
    listAllColors=[]
    for k in range(len(listColMaps)):
        try:
            listAllColors +=  [listColMaps[k](i) for i in range(listColMaps[k].N)]  
        except Exception as e:
            raise("Fatal error in get36DivergingColors color map idx ", k , " col map size: ", listColMaps[k].N, " -- ", str(e))
    
    return(listAllColors)
    
    

def getDefaultDictColQc():

    #Get the shp file 
    shpQcCity = gpd.read_file(os.path.join(ROOT_DIR, "Data","GeoData","Neighbourhoods","vdq-quartier.shp"))
    
    #Get diverging color palette
    listAllColors=get36DivergingColors()
    
    dictCol={}
    for k,row in shpQcCity.iterrows():
        dictCol[row["NOM"]] = listAllColors[k] 
    
    #Hard code the central neigh for compatibility
    dictCol["Vieux-Québec/Cap-Blanc/Colline parlementaire"] = "blue"
    dictCol["Saint-Jean-Baptiste"] = "black"
    dictCol["Saint-Roch"] = "green"
    dictCol["Saint-Sauveur"] = "teal"
    dictCol["Montcalm"] = "red"
        
    return(dictCol)


    
def assignColToShp(shp,dfColors,colMergeOn="NOM",colColorName="col"):
    
    if( np.isin( [colMergeOn,colColorName], dfColors.columns).all() ==False):
        raise Exception("Fatal error in assignColToDf! " + colMergeOn + " and " + \
                        colColorName + " are not both in dfColors")
    
    #Check if the col to merge on is in both df
    #np.isin returns an array of booleans
    isInShp=np.isin(colMergeOn, shp.columns).all()
    isInColDf=np.isin(colMergeOn, dfColors.columns).all()
    if( isInShp==False | isInColDf==False ):
        raise Exception("Fatal error in assignColToDf!" + colMergeOn , \
                        " is not in one of the df - shp :" + str(True) , \
                        " is not in one of the df - coldf :" + str(False) )
    
    #Merge
    shp=pd.merge( shp, dfColors,
                 how="left",
                 on=colMergeOn)
    
    return(shp)



def getShpQcWithCol():
    
    #Get the 36 diverging colors
    colsDefaultQc=getDefaultDictColQc()
    
    #Get the shp
    shpQcCity = gpd.read_file(os.path.join(ROOT_DIR, "Data","GeoData","Neighbourhoods","vdq-quartier.shp"))
    
    #Convert the dict to df + get the index column
    dfColsQc=pd.DataFrame.from_dict(colsDefaultQc,orient="index").reset_index()

    #Assign the NOM and col columns
    dfColsQc.rename(columns={"index":"NOM",0:"col"},inplace=True)
    
    shpQcCity=assignColToShp(shpQcCity, dfColsQc,"NOM","col")
    
    return(shpQcCity)
    
