
import seaborn as sns
import numpy as np
import pandas as pd

from OsmnxElev.OsmnxGraphGrades import *
from OsmnxElev.OsmnxGraphCreation import *

def plotEdgeGradesByNeighFromDf_all(df, listNeigh,  useAbsoluteGrade=True):
    '''

    Plot the grade distribution (density) by neighbourhood. No averaging, each neighbourhood gets its own distribution

    '''

    #Determine which column to use for grades
    gradeColName = "gradeAbsoluteVal" if useAbsoluteGrade else "grade"
    print("In plotKDEEdgeGradesByNeighFromDf_avg, considering column ", gradeColName)

    #This column is created programatically
    neighColName="Neighbourhood"
    colsTocheck=[neighColName, gradeColName ]
    if(~np.all(np.isin( colsTocheck, df.columns))):
        raise Exception("Fatal error in plotEdgeGradesByNeighFromDf_all => missing at least 1 of " + ",".join(colsTocheck))

    for neigh in listNeigh:
        idxneigh=df[neighColName]==neigh
        dat=df.loc[idxneigh,gradeColName]
        try:
            ax=sns.kdeplot(dat, label=neigh)
        except Exception as e:
            print(str(e))

    ax.set_title("Edge grades - " + "absolute value" if useAbsoluteGrade else  ""  + "\nNeighbourhoods: " + ",".join(listNeigh))

    return(ax)




def plotEdgeGradesByNeighFromDf_avg (df,listNeigh, neighColName="Neighbourhood", useAbsoluteGrade=True):
    '''

    Plot the average grade for each neighbourhood

    '''

    #Determine which column to use for grades
    gradeColName = "gradeAbsoluteVal" if useAbsoluteGrade else "grade"
    print("In plotEdgeGradesByNeighFromDf_avg, considering column ", gradeColName)

    #Quick validation
    colsTocheck = [  gradeColName]
    if (~np.all(np.isin(colsTocheck, df.columns))):
        raise Exception("Fatal error in plotEdgeGradesByNeighFromDf_avg => missing at least 1 of " + ",".join(colsTocheck))

    #Filter neighbourhoods
    idxneigh=np.isin( listNeigh, df[neighColName])
    dfFiltered=df.loc[idxneigh,:]

    #Group by neighbourhood
    dfAvgGradeByNeigh=getDfGradesGroupedNeighFromDf(dfFiltered)


    #Order by grade column
    dfAvgGradeByNeigh.sort_values(by=[gradeColName, neighColName  ], inplace=True)

    fig = sns.barplot(x=gradeColName, y=neighColName, data=dfAvgGradeByNeigh)
    sns.set(rc={'figure.figsize': (25, 8.27)})
    p = fig.get_figure()

    return(fig)


def plotEdgeGradesByNeighFromGraph_avg(graph, listNeigh, useAbsoluteGrade=True):

    dfAvgGradeByNeigh=getDfGradesGroupedNeighFromGraph(graph)

    ax=plotEdgeGradesByNeighFromDf_avg(dfAvgGradeByNeigh,listNeigh, useAbsoluteGrade= useAbsoluteGrade )

    return(ax)



def plotEdgeGradesByNeighFromGraph_all(graph, listNeigh, useAbsoluteGrade=True):

    listSubGraphs,listNodes,dfAll= getSubgraphListAllNeighWithEdgesGrade(graph)
    dfAll=dfAll.assign( gradeAbsoluteVal = lambda x :  np.abs(dfAll.grade))

    #Remove Nas
    nRowBefore=dfAll.shape[0]
    dfAllNoNas = dfAll.dropna()
    nRowAfter = dfAllNoNas.shape[0]

    ax=plotEdgeGradesByNeighFromDf_all(dfAllNoNas,listNeigh, useAbsoluteGrade= useAbsoluteGrade )

    return(ax)