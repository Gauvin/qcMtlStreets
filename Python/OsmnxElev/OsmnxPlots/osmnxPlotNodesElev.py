import matplotlib
from matplotlib import cm
import matplotlib.pyplot as plt
import matplotlib.colors as colors

import osmnx as ox

import pandas as pd
import numpy as np
import bisect
import os

from OsmnxElev import *

 
 
    
    
def plotQcMtlGraphElev(numQuantiles=20):
    
    #Get the graphs
    graphQcWithElevation = loadGraph( os.path.join(ROOT_DIR,"Data","Pickle", "graphElevationQc.pickle"),
                                      ['Québec city, canada'])
    graphMtlWithElevation =  loadGraph( os.path.join(ROOT_DIR,"Data","Pickle", "graphElevationMtl.pickle"),
                                        ['Montréal, canada'])
    
    #Get the elevations for all ndoes
    qcElev = [data["elevation"] for node, data in graphQcWithElevation.nodes(data=True)]
    mtlElev = [data["elevation"] for node, data in graphMtlWithElevation.nodes(data=True)]
    allElev=qcElev+mtlElev
    
    #Get the quantiles
    series, bins, colList = getColorBinsQuantiles(allElev, numQuantiles)
    
    #Get the colors
    listQcColors=[  getColorFromElev(bins, colList, data["elevation"]) for n,data in graphQcWithElevation.nodes(data=True) ]
    listMtlColors=[ getColorFromElev(bins, colList, data["elevation"]) for n,data in graphMtlWithElevation.nodes(data=True) ]
    
    #Plot the graphs
    qcFig=plotGraphElevation( graphQcWithElevation, listQcColors, "Quebec elevation" )
    mtlFig=plotGraphElevation( graphMtlWithElevation, listMtlColors, "Montreal elevation" )
    
    return(qcFig ,mtlFig )



def plotGraphElevation(graphWithElevation, listColors, titleStr):


    fig, ax = ox.plot_graph(graphWithElevation, 
                            fig_height=6, 
                            node_color=listColors, 
                            node_size=12, 
                            node_zorder=2, 
                            edge_color='#dddddd',
                            close=False,
                            show=False
                           )

    fig.suptitle(titleStr)


    fig = ax.get_figure()
    figName=titleStr.replace(" ", "_") + " .png"
    fig.savefig(os.path.join(ROOT_DIR,"Figures", figName))

    return(fig)