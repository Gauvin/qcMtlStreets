
import numpy as np

from OsmnxElev.OsmnxGraphCreation import *





def getDfGradesGroupedNeighFromDf(df):

    #Remove Nas
    nRowBefore=df.shape[0]
    dfNoNas = df.dropna()
    nRowAfter = dfNoNas.shape[0]

    print("In getDfEdgeGradeNeigh => removed ", nRowBefore-nRowAfter, " NA records" )

    dfAvgGradeByNeigh=dfNoNas.groupby("Neighbourhood" ).agg({"grade":"mean",
                                                               "gradeAbsoluteVal":"mean"}).reset_index()


    print("In getDfGradesGroupedNeighFromDf , here are the unique neighbouhoods:", dfAvgGradeByNeigh.Neighbourhood.unique())

    return(dfAvgGradeByNeigh)


def getDfGradesGroupedNeighFromGraph(graph):

    listSubGraphs,listNodes,dfAll= getSubgraphListAllNeighWithEdgesGrade(graph)
    dfAll=dfAll.assign( gradeAbsoluteVal = lambda x :  np.abs(dfAll.grade))

    #Ok, Neighbourhood is programtically created by getSubgraphListAllNeighWithEdgesGrade
    print("In getDfEdgeGradeNeigh, here are the neighbourhood considered in the graph: " +
          ",".join(dfAll.Neighbourhood.unique()))


    #Compute the average by neighbourhood
    dfAvgGradeByNeigh=getDfGradesGroupedNeighFromDf(dfAll)


    return(dfAvgGradeByNeigh)



