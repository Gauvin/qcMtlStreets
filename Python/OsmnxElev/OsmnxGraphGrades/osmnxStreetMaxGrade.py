
#Standard libs
import geopandas as gpd
import os
import numpy as np
from collections import defaultdict

#Local module
from OsmnxElev.OsmnxGraphCreation import *
from OsmnxElev.OsmnxCuts import *
from OsmnxElev.definitions import *


def getStreetMaxGradeForNeigh(graph, dfEdges, neigh):

    '''

    Can't use getAllListEdgesDirectedMultigraph directly with only a graph since we might need to consider edges that are between neighbourhdoods
    Edges accross neighbourhood cuts will be filtered since getAllListEdgesDirectedMultigraph only returns edges within the neigh if called with a singleton neighbourhood list

    '''

    # Columns to extract
    # Add start and end index
    # Use an immutable tuple for dictionnary keys
    keysToAdd =('startNode', 'endNode', 'startNeigh', 'endNeigh')

    # Filter the df
    idxEdgeEndPointNeigh =(dfEdges['startNeigh'] == neigh) |  (dfEdges['endNeigh'] == neigh)
    dfFilter =dfEdges.loc[ idxEdgeEndPointNeigh  ]

    # Get the edge with max grade
    dfMaxEdgeStartEnd =dfFilter.loc[ dfFilter['grade_abs'] == np.max(dfFilter['grade_abs']), list( keysToAdd ) ]
    maxEdge =dfMaxEdgeStartEnd.iloc[0, ]  # arbitrarily take the first edge

    # Loop through all edges and return the entire edge dictionnary
    # Watch out, maxEdgeDict is a list with 1 element
    maxEdgeDict =[ dat for u ,v ,dat in graph.edges.data() if (u == maxEdge['startNode']) & (v == maxEdge['endNode']) ][0]

    # Convert the pd series to a dict
    dictMaxEdgeToAdd =maxEdge.to_dict()
    dictAdd ={k :dictMaxEdgeToAdd[k] for k in keysToAdd}

    # Update the original dict
    maxEdgeDict0= maxEdgeDict.update(dictAdd)
    maxEdgeDict1= dictAdd.update(maxEdgeDict)

    returnDict= maxEdgeDict0 if maxEdgeDict1 is None else maxEdgeDict0

    return(returnDict)



def getStreetGradeMaxByNeighQc(listNeigh=["Saint-Jean-Baptiste",
                               "Vieux-Québec/Cap-Blanc/Colline parlementaire",
                               "Montcalm",
                               "Saint-Roch",
                               "Saint-Sauveur"]):

    shpQc=gpd.read_file(os.path.join(DATA_DIR,"GeoData","Neighbourhoods","vdq-quartier.shp"))

    #@TODO replace the second call by the first one => use secondonly for tests
    #graphQc=loadGraphElevGradesNeighQc()
    graphQc, shpQc, dfPointsQc  =getInducedSubgraphNeighQc(listNeigh)

    dfEdgesQc = getAllListEdgesDirectedMultigraph(graphQc,
                                                   listNeigh=shpQc.NOM,
                                                   listFeat=['grade_abs'])

    listDict=defaultdict(lambda : 0)
    for neigh in listNeigh:
        listDict[neigh]=getStreetMaxGradeForNeigh(graphQc, dfEdgesQc, neigh)

    return( listDict )