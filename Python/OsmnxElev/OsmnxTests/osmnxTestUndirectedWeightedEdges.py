

#Local modules
from OsmnxElev import definitions
from OsmnxElev.OsmnxCuts import *

def testUndirectedWeightedEdges():

    listNeigh = ["Saint-Jean-Baptiste", \
                 "Saint-Roch"]
    listFeat = ["oneway", "grade_abs", "elevation"]

    dfUndirectedEdgeListWithFeatMean = getAllUndirectedWeightedEdgesNeighGraphQc(listNeigh)
    dfUndirectedEdgeListWithFeatMean = getAllUndirectedWeightedEdgesNeighGraphQc(listNeigh, listFeat, "mean")

