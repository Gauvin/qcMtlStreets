
'''
Functions to test various functionalities

'''

#Standard lib
import os
import geopandas as gpd


#Local modules
from OsmnxElev import definitions
from OsmnxElev.OsmnxGraphCreation.osmnxCreateGraphElevGradesNeigh import *
from OsmnxElev.OsmnxGraphCreation.osmnxGetNodesInNeigh import *
from OsmnxElev.OsmnxPlots.osmnxPlotDistrEdgeGrades import *


def testPlotEdgeGrade():

    graphMtl = loadGraphElevGradesNeighMtl()
    shpMtl = gpd.read_file(
        os.path.join(ROOT_DIR, "Data", "GeoData", "MontrealNeighbourhoods", "Quartiers_sociologiques_2014.shp"))

    listNeighMtl = ['Côte-des-Neiges']

    graphQc3Neigh, shpQc3Neigh, dfPointsSubsetQc3Neigh = getInducedSubgraphNeigh(graphMtl,
                                                                                 shpMtl,
                                                                                 "Q_socio",
                                                                                 listNeighMtl)
    plotEdgeGradesByNeighFromGraph_avg(graphMtl, listNeighMtl, useAbsoluteGrade=True)