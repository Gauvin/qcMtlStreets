
#Local modules
from OsmnxElev.OsmnxGraphCreation import *
from OsmnxElev.OsmnxPlots import *
from OsmnxElev.OsmnxCuts.osmnxNeighCut import *
from OsmnxElev.OsmnxCuts.osmnxNormCut import *


def testSpectralClustering():
    computeSpectralClustFromGraphQc()

    graphQc3Neigh, shpQc3Neigh, dfPointsSubsetQc3Neigh=getInducedSubgraphNeighQc( listNeigh )
    getEdgesInSetNeigh(graphQc3Neigh, listNeigh)


    plotGraphSpecClustNeighQc(listNeigh)