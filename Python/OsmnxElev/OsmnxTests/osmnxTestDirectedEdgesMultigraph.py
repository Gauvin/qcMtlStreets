



#Local modules
from OsmnxElev import definitions
from OsmnxElev.OsmnxGraphCreation.osmnxCreateGraphElevGradesNeigh import *
from OsmnxElev.OsmnxCuts.osmnxNeighCut import *


def testEdgesDirectedMultigraphDf(listNeigh = ["Saint-Jean-Baptiste", 'Saint-Roch']):


    graphSJB, shpQcSJB, dfPointsSubsetQcSJB = getInducedSubgraphNeighQc(listNeigh)




    # Get all edges
    dfEdgesSJB = getAllListEdgesDirectedMultigraph(graphSJB,
                                                    listNeigh=listNeigh,
                                                    listFeat=['grade_abs', 'bearing'])


    return True


def testEdgesDirectedMultigraphAdjMatrices( listNeigh = ["Saint-Jean-Baptiste", 'Saint-Roch'],
                                            listFeat=['grade_abs', 'bearing'] ):


    graphQc = loadGraphElevGradesNeighQc()

    dfListEdgesMultigraph = getAllListEdgesDirectedMultigraph(graphQc, listNeigh, listFeat)

    getAdjMatrixFromEdgeList(dfListEdgesMultigraph,
                             nodeNameIn="startNeigh",
                             nodeNameOut="endNeigh",
                             useDirected=False,
                             weightCol=None)

    getAdjMatrixFromEdgeList(dfListEdgesMultigraph,
                             useDirected=False,
                             weightCol="grade_abs",
                             aggFun="mean")



