#Local modules
from OsmnxElev import definitions
from OsmnxElev.OsmnxCuts.osmnxNeighCut import *

def testPlotEdgeBearing():

    #Compute edges vs bearings
    plotBearingVsGrade(dfEdgesSJB, ('Saint-Jean-Baptiste', 'Saint-Roch'))