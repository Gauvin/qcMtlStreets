import matplotlib
from matplotlib import cm
import matplotlib.pyplot as plt
import matplotlib.colors as colors

import bisect

import os



def getColorFromElev(bins,elev):
    try:
        idx=bisect.bisect_left(bins, elev)  #watch out with the indexing, bisect.bisect_left(bins, elev) returns values in [1,20] 
        c=colors[idx-1]
    except Exception as e:
        print("Fatal error at elevation ", elev, " idx ", idx)
        
    return(c)
    
    
 
    
    
def plotQcMtlGraphElev(numQuantiles=20):
    
    #Get the graphs
    graphQcWithElevation = loadGraph( os.path.join(ROOT_DIR,"Data","Pickle", "graphElevationQc.pickle"))
    graphMtlWithElevation =  loadGraph( os.path.join(ROOT_DIR,"Data","Pickle", "graphElevationMtl.pickle"))
    
    #Get the elevations for all ndoes
    qcElev = [data["elevation"] for node, data in graphQcWithElevation.nodes(data=True)]
    mtlElev = [data["elevation"] for node, data in graphMtlWithElevation.nodes(data=True)]
    allElev=qcElev+mtlElev
    
    #Get the quantiles
    ser, bins = pd.qcut(x=allElev, 
                        q=numQuantiles, 
                        labels=np.linspace(1,numQuantiles,numQuantiles), 
                        retbins=True)
    
    #Get the colors
    listQcColors=[  getColorFromElev(bins, data["elevation"]) for n,data in graphQcWithElevation.nodes(data=True) ]
    listMtlColors=[ getColorFromElev(bins, data["elevation"]) for n,data in graphMtlWithElevation.nodes(data=True) ]
    
    #Plot the graphs
    plotGraphElevation( graphQcWithElevation, listQcColors, "Quebec elevation" )
    plotGraphElevation( graphMtlWithElevation, listMtlColors, "Montreal elevation" )
    
    
def plotGraphElevation(graphWithElevation, listColors, titleStr):


    fig, ax = ox.plot_graph(graphWithElevation, 
                            fig_height=6, 
                            node_color=listColors, 
                            node_size=12, 
                            node_zorder=2, 
                            edge_color='#dddddd',
                            close=False,
                            show=False
                           )

    fig.suptitle(titleStr)


    fig = ax.get_figure()
    figName=titleStr.replace(" ", "_") + " .png"
    fig.savefig(os.path.join(ROOT_DIR,"Figures", figName))