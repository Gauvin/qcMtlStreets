import re
import numpy an np
import pandas as pd
import geopandas as gpd
import os

from shapely.geometry import Point
from shapely.geometry import Polygon, MultiPolygon
from descartes import PolygonPatch
from matplotlib.collections import PatchCollection

from OsmxElev.definition import *



def isPointInPolygon(point,poly):
    return( poly.contains( point ) )
           
def isPointInGroupPolygons(point,listPoly):
    return( np.any( [ isPointInPolygon(point,poly) for poly in listPoly ] ) ) 

def getIdxPointInPolygon(listPoints,poly):
    return(  [ isPointInPolygon(point,poly) for point in listPoints ] ) 

def getIdxListPointsInGroupPolygons(listPoints,listPoly):
    return( [ isPointInGroupPolygons(point, listPoly ) for point in listPoints] )



def getPointNeighbourhoodAdjList(listPoints, dfNeigh):

    dfPointsPolyAdjList=pd.DataFrame()

    for k,poly in enumerate(dfNeigh["geometry"]):
        name=dfNeigh["NOM"][k]
        dfPointsPolyAdjList[name]= getIdxPointInPolygon(listPoints, poly )
        
    return(dfPointsPolyAdjList)


def getPointIndexInNeigh(dfPointsPolyAdjList, neigh):
    
    return( dfPointsSubsetInPoly.loc[ dfPointsPolyAdjList[neigh], "nodeIndex"].values )



def setPointNeighbourhood(graph, listNodes, neigh):
    listRet=[]
    for k,dat in enumerate(graph.nodes.data()):
        try:
            if np.isin( dat[0] ,listNodes):
                dat[1]["Neighbourhood"] = neigh
                listRet.append(dat[0])
        except Exception as e:
            print("Fatal error in setPointNeighbourhood -- at neigh ", neigh, " node index: ", k)

    return(listRet)

def setPointAllNeighbourhood(graph, listPoints, dfNeigh):
    
    dfPointsPolyAdjList=getPointNeighbourhoodAdjList(listPoints, dfNeigh)
    dictListNodes={}
        
    for k in range(dfNeigh.shape[0]):
        try:
            neigh= dfNeigh.loc[k,"NOM"]
            listNodes=  getPointIndexInNeigh(dfPointsPolyAdjList,neigh)
            listRet=setPointNeighbourhood(graph, listNodes, neigh)
            dictListNodes[neigh] = listRet
        except Exception as e:
            print("Fatal error in setPointAllNeighbourhood -- at neigh ", neigh)
            
    return(dictListNodes)


def getListNeighGraphs(graph, shpNeigh, listNeigh,varNameID="NOM"):
    
    listGraphs=[]
   
    #Select only the desired neighbourhoods
    if listNeigh is not None:
        shpNeighFiltered=shpNeigh[ np.isin(shpNeigh[varNameID],listNeigh) ]
    else:
        shpNeighFiltered=shpNeigh
        
        
    graphQcinduced=ox.utils.induce_subgraph(graphQc ,  dfPointsSubsetInPoly["nodeIndex"].values )
        
    #Add the neighbourhood to all the nodes
    dictListNodes=setPointAllNeighbourhood(graphQcinduced, dfPointsSubsetInPoly["geometry"], shpNeighFiltered )
    
    #Loop through all neighbourhoods and create the induced subgraph
    for k,v in dictListNodes.items():
        name=re.sub("[/-]","_",k)
        fileNameWithPath = os.path.join(ROOT_DIR,"Data","Pickle","graphStreetsByNeigh", name + ".pickle")
        
        if !os.file.exists(fileNameWithPath):
            print(fileNameWithPath + " does not exist => creating the induced subgraph")
            inducedSubgraph=ox.utils.induce_subgraph(graphQc,v)
            
            with open(fileNameWithPath,'wb') as fileHandle:
                pickle.dump(file=fileHandle,
                       obj=inducedSubgraph)
        else:
             with open(fileNameWithPath,'rb') as fileHandle:
                inducedSubgraph=pickle.load(fileHandle)
                
        listGraphs.append(inducedSubgraph)
        
    return(listGraphs)


        
       
            
def getListNeighGraphsQc(listNeigh=[ "Saint-Jean-Baptiste","Vieux-Québec/Cap-Blanc/Colline parlementaire","Saint-Roch"]):
    
    place=['Québec city, canada']

    #osmnx graph
    graphQc = ox.graph_from_place(place, 
                           simplify=False,
                           retain_all=True)
    
    #neighbourhood shape file
    qcCityNeigh = gpd.read_file(os.path.join(ROOT_DIR, "Data","GeoData","Neighbourhoods","vdq-quartier.shp"))
    
    
    listGraphs=getNeighGraphs(graphQc ,qcCityNeigh, listNeigh)
    
    
    return(listGraphs)
    

